//
//  AppEnvironment.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation

#if os(macOS)
import AppKit
#endif

protocol AppEnvironmentProvider {
    var dataManager: DataManager! { get }
    var sessionManager: SessionManager! { get }
}

struct AppEnvironment {
    static var shared: AppEnvironmentProvider {
        #if os(macOS)
        // swiftlint:disable force_cast
        return NSApp.delegate as! AppDelegateMac
        #else
        preconditionFailure("we need to add some environment provider here")
        #endif
    }
}
