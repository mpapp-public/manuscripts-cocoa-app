//
//  CredentialsView.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import SwiftUI
import Combine

enum CredentialEntryState: Equatable {
    case idle
    case requested(String?)
    case submitted(Credentials)

    var isSubmitted: Bool {
        switch self {
        case .submitted:
            return true
        default:
            return false
        }
    }
}

struct ValidationError: Swift.Error {
    let message: String
}

struct CredentialsView: View {
    @ObservedObject var sessionManager = AppEnvironment.shared.sessionManager

    @State var username: String = ""
    @State var password: String = ""

    var body: some View {
        var message: String?
        if case let .requested(msg) = sessionManager.credentialState {
            message = msg
        }

        return VStack(alignment: .trailing, spacing: 10) {
            Text(message ?? "").foregroundColor(.red)
            #if os(iOS)
            if sessionManager.currentUsername == nil {
                TextField("Username", text: $username)
                    .frame(minWidth: 200)
                    .textFieldStyle(RoundedBorderTextFieldStyle())
                    .textContentType(.emailAddress)
                    .keyboardType(.emailAddress)
                    .autocapitalization(.none)
                    .disabled(sessionManager.currentUsername != nil)
            } else {
                Text(sessionManager.currentUsername!)
            }
            #else
            if sessionManager.currentUsername == nil {
                TextField("Username", text: $username)
                    .frame(minWidth: 200)
                    .disabled(sessionManager.currentUsername != nil)
            } else {
                Text(sessionManager.currentUsername!)
            }
            #endif
            #if os(iOS)
            SecureField("Password", text: $password)
                .frame(minWidth: 200)
                .textFieldStyle(RoundedBorderTextFieldStyle())
                .textContentType(.password)
                .autocapitalization(.none)
            #else
            SecureField("Password", text: $password)
                .frame(minWidth: 200)
            #endif
            HStack {
                Button(action: {
                    self.sessionManager.cancelLogin()
                }, label: {
                    Text("Cancel")
                })
                Button(action: {
                    self.sessionManager.submitCredentials(Credentials(username: self.sessionManager.currentUsername ?? self.username, password: self.password))
                }, label: {
                    Text("Log In")
                })
            }
        }
        .disabled(sessionManager.credentialState.isSubmitted)
        .padding()
        .frame(idealWidth: 250)
    }
}

#if DEBUG

struct CredentialsPreview: View {
    var body: some View {
        CredentialsView()
    }

}

struct CredentialsViewPreviews: PreviewProvider {
    static var previews: some View {
        CredentialsPreview()
    }
}

#endif
