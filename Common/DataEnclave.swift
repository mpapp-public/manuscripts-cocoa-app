//
//  DataEnclave.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import Combine
import os.log

/**
 Base class wrapping all data related to a single data scope (ATM local or cloud).
 Creates manages and resets a `DocumentServer` instance and WebKit local data (LocalDB and IndexedDB).
 */
class DataEnclave {

    enum Scope: String {
        case local
        case standalone
        case cloud

        var editorBaseUrl: URL {
            switch self {
            case .cloud:
                return URL(string: Configuration.standard.editorCloudBaseUrl)!
            case .local:
                return URL(string: "app-resources://manuscripts.local")!
            case .standalone:
                return URL(string: "app-resources://standalone.local")!
            }
        }

        var dataDirectoryName: String {
            switch self {
            case .cloud:
                return "CloudData"
            case .local:
                return "LocalData"
            case .standalone:
                return "StandaloneData"
            }
        }
    }

    var documentServerType: DocumentServer.Type { preconditionFailure("requested an abstract class") }
    private(set) var scope: Scope
    let sessionManager: SessionManager

    private(set) var documentServer: DocumentServer!

    required init(scope: Scope, sessionManager: SessionManager) throws {
        self.sessionManager = sessionManager
        self.scope = scope
        self.documentServer = try createDocumentServer()
    }

    var dataDirectoryUrl: URL {
        guard let url = try? FileManager.default.url(for: .applicationSupportDirectory, in: .userDomainMask, appropriateFor: nil, create: true).appendingPathComponent("\(Bundle.main.bundleIdentifier!)/\(scope.dataDirectoryName)")
        else {
                preconditionFailure("could not enclave determine data directory")
        }
        return url
    }

    final func ensureDataDirectoryUrl() throws -> URL {
        let url = dataDirectoryUrl
        try FileManager.default.createDirectory(at: url, withIntermediateDirectories: true, attributes: nil)
        return url
    }

    fileprivate func documentServerConfiguration(for sessionState: SessionState) throws -> DocumentServer.Configuration {
        preconditionFailure("abstract method called")
    }

    final func createDocumentServer() throws -> DocumentServer {
        let configuration = try documentServerConfiguration(for: sessionManager[scope])
        return try documentServerType.init(enclave: self, configuration: configuration)
    }

    var sessionStateSink: AnyCancellable?

    func start() {
        documentServer.start()
        sessionStateSink = watchSessionState()
    }

    func stop() {
        documentServer.stop()
        sessionStateSink?.cancel()
        sessionStateSink = nil
    }

    // MARK: - Session Management
    private func watchSessionState() -> AnyCancellable {
        return sessionManager.sessionStatePublisher(for: scope)
            .removeDuplicates()
            .sink { [weak self] nextSessionState in
                self?.reflectNextSessionState(nextSessionState)
            }
    }

    /**
     Removes known folders where the WebKit stores IndexedDB and LocalDB data.

     `WKWebsiteDataStore.default().fetchDataRecords` unfortunately does not work properly for non-http(s) origins
     hence we need to erase the data directly and hope WebKit does not hold any reference to this data.

     Issue reported here: https://bugs.webkit.org/show_bug.cgi?id=205450
     */
    final func resetWebKitData() throws {
        let webKitManager = WebKitDataManager(url: scope.editorBaseUrl)
        try webKitManager.reset()
    }

    /// Removes all related data - CBL databases and WebKit LocalDB and IndexedDB.
    final func reset(to sessionState: SessionState) throws {
        try resetWebKitData()
        try documentServer.reset(to: try self.documentServerConfiguration(for: sessionState))
    }

    /// Returns true if session sate processing should be considered as complete.
    @discardableResult
    fileprivate func reflectNextSessionState(_ nextSessionState: SessionState) -> Bool {
        let sessionDiff = nextSessionState.diff(from: sessionManager[scope])
        if sessionDiff.contains(.user) {
            do {
                try reset(to: nextSessionState)
                return true
            } catch let error {
                os_log(.error, "error resetting data: %{public}@", error.localizedDescription)
            }
        }
        return false
    }

}

class LocalDataEnclave: DataEnclave {

    override var documentServerType: DocumentServer.Type { LocalDocumentServer.self }

    override fileprivate func documentServerConfiguration(for sessionState: SessionState) throws -> DocumentServer.Configuration {
        let dataDirectoryUrl = try ensureDataDirectoryUrl()
        return DocumentServer.Configuration(dataDirectory: dataDirectoryUrl, remote: nil)
    }

}

class CloudDataEnclave: DataEnclave {
    static private let primaryBucketName = "projects"
    static private let derivedDataBucketName = "derived_data"

    override var documentServerType: DocumentServer.Type { CloudDocumentServer.self }

    override fileprivate func documentServerConfiguration(for sessionState: SessionState) throws -> DocumentServer.Configuration {
        let serverURL = URL(string: Configuration.standard.replicationBaseUrl)!

        let dataDirectoryUrl = try ensureDataDirectoryUrl()

        let remoteConfiguration: DocumentServer.RemoteConfiguration?
        if let username = sessionState.user?.username {
            remoteConfiguration = DocumentServer.RemoteConfiguration(username: username, syncGatewayBaseUrl: serverURL, primaryBucketName: Self.primaryBucketName, derivedDataBucketName: Self.derivedDataBucketName, cookies: sessionState.gatewayCookies)
        } else {
            remoteConfiguration = nil
        }
        return DocumentServer.Configuration(dataDirectory: dataDirectoryUrl, remote: remoteConfiguration)
    }

    override func start() {
        guard let server = documentServer as? CloudDocumentServer else {
            preconditionFailure("unexpected document server type")
        }
        server.errorHandler = { _, _, error in
            //if we want to use other closure params related to the CBMManager
            //we need to do it on the callers thread/queue before we switch to the main queue
            DispatchQueue.main.async {
                switch error {
                case let DocumentServer.Error.underlyingError(error: underlyingError):
                    switch underlyingError {
                    case let nsError as NSError where nsError.domain == "CBLHTTP" && nsError.code == 401:
                        os_log(.error, "sync session requires authentication", nsError)
                        self.sessionManager.refreshSyncGatewayCookies()
                    default:
                        os_log(.error, "replication error \"%{public}@\"", error.localizedDescription)
                    }
                default:
                    os_log(.error, "replication error \"%{public}@\"", error.localizedDescription)
                }
            }
        }

        server.statusUpdateHandler = { (database, replication) in
            //we are on the database manager queue here
            os_log(.debug, "database replication %{public}@ (%{public}@) status changed status=%{public}@, completed=%d, total=%d",
                database.name, replication.replicationType.debugDescription, replication.status.debugDescription, replication.completedChangesCount, replication.changesCount)
        }

        //starts all replications. async
        super.start()
    }

    override fileprivate func reflectNextSessionState(_ nextSessionState: SessionState) -> Bool {
        if super.reflectNextSessionState(nextSessionState) { return true }

        let sessionDiff = nextSessionState.diff(from: sessionManager[scope])
        if sessionDiff.contains(.cookies) {
            //async
            self.documentServer.applyBucketCookies(nextSessionState.gatewayCookies)
        }
        return true
    }
}
