//
//  SessionState.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import JWT

public typealias GatewayCookies = [String: HTTPCookie]

/**
 `SessionState` keeps all information about the logged-in user.

 In addition to user identity it optionally contains (token) or (token, cookies). `SessionManager` keeps this information up-to-date. Cookies and token may be unset when the `SessionManager` recognises they are not longer valid - but it still ensures the session user is available.
 */
struct SessionState: Equatable {

    struct DiffResult: OptionSet {
        let rawValue: Int

        static let user = DiffResult(rawValue: 1 << 0)
        static let token = DiffResult(rawValue: 1 << 1)
        static let cookies = DiffResult(rawValue: 1 << 2)
    }

    struct User: Equatable {
        private static let usernameKey = "username"
        private static let userIdKey = "userId"
        private static let userProfileIdKey = "userProfileId"

        let username: String
        let userId: String
        let userProfileId: String

        init(username: String, userId: String, userProfileId: String) {
            self.username = username
            self.userId = userId
            self.userProfileId = userProfileId
        }

        init?(dictionary: [String: Any]) {
            guard
                let dictUsername = dictionary[Self.usernameKey] as? String,
                let dictUserId = dictionary[Self.userIdKey] as? String,
                let dictUserProfileId = dictionary[Self.userProfileIdKey] as? String
            else {
                return nil
            }
            self.username = dictUsername
            self.userId = dictUserId
            self.userProfileId = dictUserProfileId
        }

        var dictionary: [String: Any] {
            [Self.usernameKey: username, Self.userIdKey: userId, Self.userProfileIdKey: userProfileId]
        }
    }

    static let void = SessionState(user: nil, token: nil, gatewayCookies: nil)

    private static let userKey = "user"
    private static let tokenKey = "token"
    private static let gatewayCookiesKey = "gatewayCookies"

    let user: User?
    let token: String?
    let gatewayCookies: GatewayCookies?

    private static let syncGatewayCookieName = "SyncGatewaySession"
    private static let syncGatewayBuckets = ["derived_data", "projects"]

    static func syncGatewayCookies(from allCookies: [HTTPCookie]) throws -> GatewayCookies {
        let cookies = allCookies
            .filter({$0.name == Self.syncGatewayCookieName})
            .reduce(into: GatewayCookies()) { (result, cookie) in
                guard cookie.path.hasPrefix("/") else {
                    return
                }
                let indexFrom = cookie.path.index(after: cookie.path.startIndex)
                result[String(cookie.path[indexFrom...])] = cookie
            }
        guard Set(cookies.keys) == Set(Self.syncGatewayBuckets) else {
            throw APIError.invalidCookies
        }
        return cookies
    }

    private init(user: User?, token: String? = nil, gatewayCookies: GatewayCookies? = nil) {
        self.user = user
        self.token = token
        self.gatewayCookies = gatewayCookies
    }

    init(token: String, gatewayCookies: GatewayCookies? = nil) throws {
        let claims = try JWT.decode(token, algorithm: .none, verify: false)
        guard
            let jwtUsername = claims["email"] as? String,
            let jwtUserId = claims["userId"] as? String,
            let jwtUserProfile = claims["userProfileId"] as? String
            else {
                throw APIError.invalidJWT
        }
        let jwtUser = User(username: jwtUsername, userId: jwtUserId, userProfileId: jwtUserProfile)
        self.user = jwtUser
        self.token = token
        self.gatewayCookies = gatewayCookies
    }

    init?(dictionary: [String: Any]) {
        guard
            let userDictionary = dictionary[Self.userKey] as? [String: Any],
            let storedUser = User(dictionary: userDictionary)
        else {
            return nil
        }
        self.user = storedUser

        self.token = dictionary[Self.tokenKey] as? String
        var storedCookies: GatewayCookies?
        if self.token != nil {
            if let cookiesDict = dictionary[Self.gatewayCookiesKey] as? [[String: Any]] {
                let cookies = cookiesDict.compactMap { cookieDict -> HTTPCookie? in
                    let props = cookieDict.reduce(into: [:]) { (result, pair) in
                        result[HTTPCookiePropertyKey(rawValue: pair.key)] = pair.value
                    }
                    return HTTPCookie(properties: props)
                }
                storedCookies = try? Self.syncGatewayCookies(from: cookies)
            }
        }
        self.gatewayCookies = storedCookies
    }

    var dictionary: [String: Any]? {
        guard let realUser = user else { return nil }

        var ret = [String: Any]()
        ret[Self.userKey] = realUser.dictionary
        ret[Self.tokenKey] = token
        if let cookiesArray = gatewayCookies?.values.map({$0.properties}) {
            ret[Self.gatewayCookiesKey] = cookiesArray
        }
        return ret
    }

    func resettingToken() -> SessionState {
        SessionState(user: user, token: nil, gatewayCookies: nil)
    }

    func resettingCookies(_ cookies: GatewayCookies? = nil) -> SessionState {
        SessionState(user: user, token: token, gatewayCookies: cookies)
    }

    func diff(from other: SessionState) -> DiffResult {
        var result: DiffResult = []
        if user != other.user { result.insert(.user) }
        if token != other.token { result.insert(.token) }
        if gatewayCookies != other.gatewayCookies { result.insert(.cookies) }
        return result
    }
}
