//
//  FlatButton.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import SwiftUI

struct FlatButton: View {

    let title: String
    let color: Color
    let invertedColor: Color
    let action: () -> Void

    @State private var isHovered = false

    var body: some View {
        GeometryReader { geo in
            Button(action: {self.action()}, label: {
                Text(self.title)
                    .frame(width: geo.size.width, height: geo.size.height)
                    .font(.system(size: 16))
                    .foregroundColor(self.isHovered ? self.color : self.invertedColor)
                    .background(
                        ZStack {
                            RoundedRectangle(cornerRadius: 8)
                                .fill(self.isHovered ? self.invertedColor : self.color)
                            RoundedRectangle(cornerRadius: 8)
                                .stroke(Color(red: Double(13) / 255, green: Double(121) / 255, blue: Double(208) / 255))
                        })
                // TODO: https://gitlab.com/mpapp-public/manuscripts-cocoa-app/issues/54
                /*
                    .onHover(perform: { value in
                        withAnimation {
                            self.isHovered = value
                        }
                    })
                 */
            })
            .buttonStyle(PlainButtonStyle())
        }
    }
}
struct FlatButtonPreviews: PreviewProvider {
    static var previews: some View {
        FlatButton(title: "Test", color: .blue, invertedColor: .white, action: {})
            .frame(width: 300, height: 50)
    }
}
