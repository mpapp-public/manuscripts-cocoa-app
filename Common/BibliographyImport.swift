//
//  BibliographyImport.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import Combine

struct BibliographyImportOptions: OptionSet {
    let rawValue: Int
    static let safeMode = Self.init(rawValue: 1 << 0)
    static let preferBulkMode = Self.init(rawValue: 1 << 1)
}

enum BibliographyDataFormat {
    case citeprocJson

    var contentType: String {
        switch self {
        case .citeprocJson:
            return "application/citeproc+json"
        }
    }
}

struct BibliogaphyImportResult {
    let format: BibliographyDataFormat
    let importedItems: [Data]
    let importErrors: [Error]
}

// MARK: -
enum BibliographyImportError: RetryableError {
    case timeout
    case retryable(underlayingError: Error)
    case importError(underlayingError: Error)
    case unknownError(underlayingError: Error)
    case invalidData
    case fatalFailure(String)

    var isRetryable: Bool {
        switch self {
        case .retryable:
            return true
        default:
            return false
        }
    }

    static func wrapErrorConditionally(_ error: Error) -> Self {
        switch error {
        case let selfTyped as BibliographyImportError:
            return selfTyped
        case let retryable as RetryableError:
            return retryable.isRetryable ? .retryable(underlayingError: retryable) : .importError(underlayingError: retryable)
        default:
            return .importError(underlayingError: error)
        }
    }
}

typealias BibliographyImportCompletion = (Result<BibliogaphyImportResult, BibliographyImportError>) -> Void
typealias BibliographyApplyCompletion = (Result<[NativeBridgeResult], BibliographyImportError>) -> Void

/**
 Protocol adopted by concrete bibliography importers.
 Used directly or by a BibliographyImportInteractive task.
 */
protocol BibliographyImport: class {
    static var supportsSafeMode: Bool { get }

    /// Asynchronous import. Typically an XPC call
    func importLibrary(progress: Progress?, options: BibliographyImportOptions, completion: @escaping BibliographyImportCompletion)

    /// Import from a file. XPC based imports may use an intermediate file too.
    func importLibrary(from fileUrl: URL, progress: Progress?, options: BibliographyImportOptions, completion: @escaping BibliographyImportCompletion)

    /// Publisher providing BibliogaphyImportResult. Handles retryable errors.
    func importLibraryPublisher(from fileUrl: URL?, progress: Progress?, options: BibliographyImportOptions, timeout: DispatchQueue.SchedulerTimeType.Stride, retries: Int) -> AnyPublisher<BibliogaphyImportResult, BibliographyImportError>

    /// The final step of import - imported result is send to the editor via the native bridge.
    func applyImportResult(_ importResult: BibliogaphyImportResult, nativeBridge: NativeBridge, progress: Progress?, options: BibliographyImportOptions, completion: @escaping BibliographyApplyCompletion)

    func cancel()
}

extension BibliographyImport {
    static var supportsSafeMode: Bool { false }

    func importLibrary(from fileUrl: URL, progress: Progress?, options: BibliographyImportOptions, completion: @escaping BibliographyImportCompletion) {
        completion(.failure(.fatalFailure("not implemented")))
    }

    func importLibrary(progress: Progress?, options: BibliographyImportOptions, completion: @escaping BibliographyImportCompletion) {
        completion(.failure(.fatalFailure("not implemented")))
    }

    func canSafelyRetry(error: BibliographyImportError) -> Bool {
        false
    }

    func cancel() {
        //noop
    }

    func applyImportResult(_ importResult: BibliogaphyImportResult, nativeBridge: NativeBridge, progress: Progress?, options: BibliographyImportOptions, completion: @escaping BibliographyApplyCompletion) {
        completion(.failure(.fatalFailure("not implemented")))
    }

    /**
     Creates a publisher for a bibliography import provided by a BibliographyImportProvider instance.
     */
    func importLibraryPublisher(from fileUrl: URL? = nil, progress: Progress? = nil, options: BibliographyImportOptions = [], timeout: DispatchQueue.SchedulerTimeType.Stride = 3600, retries: Int = 0) -> AnyPublisher<BibliogaphyImportResult, BibliographyImportError> {
        return Just(true)
            .setFailureType(to: BibliographyImportError.self)
            //this is kind of violent, but needed to break the retain cycle of non-owned sink reference reliably
            .flatMap { [weak self] _ in self!.importFuture(fileUrl: fileUrl, progress: progress, options: options) }
            //retry retryable errors emitted by the provided
            .retry(retries)
            //fail for non-retryable errors returned from the provider
            .tryMap { (tuple) -> BibliogaphyImportResult in
                guard tuple.1 == nil else { throw tuple.1! }
                guard let result = tuple.0 else { throw BibliographyImportError.fatalFailure("value not found where expected") }
                return result
            }
            //back to typed error
            .mapError { error -> BibliographyImportError in
                return BibliographyImportError.wrapErrorConditionally(error)
            }
            //.timeout always delivers on its scheduler (even if there is no timeout)
            .timeout(timeout, scheduler: DispatchQueue.main) { return BibliographyImportError.timeout }
            .eraseToAnyPublisher()
    }

    /**
     This future triggers an error only if it's retryable and allows .retry() to handle it. If the error is fatal it's returned as a part of the result and later thrown from .tryMap. This allows us not to retry fatal errors.
     */
    private func importFuture(fileUrl: URL? = nil, progress: Progress?, options: BibliographyImportOptions = []) -> AnyPublisher<(BibliogaphyImportResult?, BibliographyImportError?), BibliographyImportError> {
        if let fromUrl = fileUrl {
            return Future<(BibliogaphyImportResult?, BibliographyImportError?), BibliographyImportError> { [weak self] promise in
                self?.importLibrary(from: fromUrl, progress: progress, options: options) { (result) in
                    switch result {
                    case let .failure(error):
                        if error.isRetryable {
                            promise(.failure(error))
                        } else {
                            promise(.success((nil, error)))
                        }
                    case let .success(importResult):
                        promise(.success((importResult, nil)))
                    }
                }
            }
            .handleEvents(receiveCancel: {[weak self] in self?.cancel()})
            .eraseToAnyPublisher()
        } else {
            return Future<(BibliogaphyImportResult?, BibliographyImportError?), BibliographyImportError> { [weak self] promise in
                self?.importLibrary(progress: progress, options: options) { (result) in
                    switch result {
                    case let .failure(error):
                        if error.isRetryable {
                            promise(.failure(error))
                        } else {
                            promise(.success((nil, error)))
                        }
                    case let .success(importResult):
                        promise(.success((importResult, nil)))
                    }
                }
            }
            .handleEvents(receiveCancel: {[weak self] in self?.cancel()})
            .eraseToAnyPublisher()
        }
    }

}
