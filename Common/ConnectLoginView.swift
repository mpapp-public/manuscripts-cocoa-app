//
//  ConnectLoginView.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import SwiftUI
import os.log

struct ConnectLoginView: View {

    class ConnectState: ObservableObject {
        let sessionManager: SessionManager

        @Published var askPurgeConsent = false

        private var token: String?
        private var cookies: [String: HTTPCookie]?

        init(sessionManager: SessionManager) {
            self.sessionManager = sessionManager
        }

        func cancelLogin() {
            sessionManager.cancelLogin()
        }

        func setLoginToken(_ token: String, cookies: [String: HTTPCookie]?) {
            do {
                self.token = token
                self.cookies = cookies
                if !(try sessionManager.acceptLoginToken(token, cookies: cookies)) {
                    withAnimation {
                        askPurgeConsent = true
                    }
                }
            } catch let error {
                // TODO: #36 present the error somehow (sheet?)
                os_log(.error, "error setting login token and cookies: %{private}@", error.localizedDescription)
            }
        }

        func confirmTokenAndCookies() {
            guard let storedToken = token else {
                preconditionFailure("called force token while not having one")
            }
            do {
                _ = try sessionManager.acceptLoginToken(storedToken, cookies: cookies, allowAccountSwitch: true)
            } catch let error {
                // TODO: #36 present the error somehow (sheet?)
                os_log(.error, "error setting login token and cookies: %{private}@", error.localizedDescription)
            }
        }

    }

    @EnvironmentObject var state: ConnectState
    @State var webViewLoaded = false

    var body: some View {
        VStack(spacing: 0.0) {
            if state.askPurgeConsent {
                HStack {
                    Spacer()
                    VStack {
                        Spacer().layoutPriority(1.0)
                        Text("You may have unsynced changes")
                            .font(.headline)
                        Text("If you sign out, your changes will be lost")
                            .font(.subheadline)
                            .padding()
                            .fixedSize(horizontal: false, vertical: true)
                            .lineLimit(10)
                            .multilineTextAlignment(.center)
                        Spacer(minLength: 30.0).layoutPriority(0.5)
                        FlatButton(title: "Switch account now", color: .buttonBlue, invertedColor: .white) {
                            self.state.confirmTokenAndCookies()
                        }
                        .frame(width: 304, height: 48)
                        Spacer().layoutPriority(1.0)
                    }
                    Spacer()
                }
                .background(Color.white)
                .transition(.asymmetric(insertion: .move(edge: .trailing), removal: .move(edge: .leading)))
            } else {
                ZStack {
                    Text("Loading…")
                        .font(.largeTitle)
                    #if os(iOS)
                    #error("follow pattern of EditorWebView(Mac) to implement this")
                    #else
                    ConnectLoginWebViewMac(webViewLoaded: $webViewLoaded)
                        .opacity(webViewLoaded ? 1.0 : 0.0)
                        .animation(.easeInOut)
                    #endif
                }
                .background(Color.white)
                .transition(.asymmetric(insertion: .move(edge: .leading), removal: .move(edge: .trailing)))
            }
            ZStack {
                HStack(alignment: .center) {
                    Spacer()
                    VStack(spacing: 8) {
                        Text("Or Continue Without Signing In")
                            .foregroundColor(Color(red: 0.24, green: 0.73, blue: 0.98))
                            .underline()
                            .onTapGesture {
                                self.state.cancelLogin()
                            }
                        Text("You can edit only locally created documents without signing in.")
                            .foregroundColor(Color(white: 0.75))
                    }
                    Spacer()
                }
            }
            .padding(15)
            .background(Color.white)
            .clipped()
            .shadow(color: Color(white: 0.8), radius: 5, x: 0, y: -2)
        }
        .font(Font.custom("Lato", size: 14))
        .frame(width: 500, height: 680)
    }
}

struct ConnectLoginViewPreviews: PreviewProvider {
    static var previews: some View {

        let state = ConnectLoginView.ConnectState(sessionManager: SessionManager())
        state.askPurgeConsent = false

        return ConnectLoginView()
            .environmentObject(state)
    }
}
