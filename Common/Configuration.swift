//
//  Configuration.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation

struct Configuration: Decodable {
    let apiBaseUrl: String
    let replicationBaseUrl: String
    let editorCloudBaseUrl: String
    let applicationSecret: String

    static let configurationFileInfoPlistKey = "ConfigurationFile"

    static let standard: Configuration = {
        guard let configFileName = Bundle.main.infoDictionary?[configurationFileInfoPlistKey] as? String else {
            preconditionFailure("missing Info.plist \(configurationFileInfoPlistKey) key with a string value")
        }
        guard let jsonUrl = Bundle.main.url(forResource: configFileName, withExtension: nil) else {
            preconditionFailure("configuration file '\(configFileName)' not found in the application bundle")
        }
        do {
            let jsonData = try Data(contentsOf: jsonUrl)
            let decoder = JSONDecoder()
            return try decoder.decode(Configuration.self, from: jsonData)
        } catch let err {
            preconditionFailure("could not load and parse configuration file: \(err)")
        }
    }()
}
