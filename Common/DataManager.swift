//
//  DataManager.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation

/**
 Central hub for all data management. It instantiates and runs data enclaves for local and cloud projcts.
 */
class DataManager: ObservableObject {

    private var sessionManager: SessionManager

    private(set) var localEnclave: DataEnclave!
    private(set) var standaloneEnclave: DataEnclave!
    private(set) var cloudEnclave: DataEnclave!

    subscript(scope: DataEnclave.Scope) -> DataEnclave {
        switch scope {
        case .local:
            return localEnclave
        case .standalone:
            return standaloneEnclave
        case .cloud:
            return cloudEnclave
        }
    }

    init(sessionManager: SessionManager) throws {
        self.sessionManager = sessionManager

        localEnclave = try LocalDataEnclave(scope: .local, sessionManager: sessionManager)
        standaloneEnclave = try LocalDataEnclave(scope: .standalone, sessionManager: sessionManager)
        cloudEnclave = try CloudDataEnclave(scope: .cloud, sessionManager: sessionManager)

        // TODO: possibly this is not needed if we support state restoration
        //try standaloneEnclave.reset(to: sessionManager.localSessionState)

        localEnclave.start()
        standaloneEnclave.start()
        cloudEnclave.start()
    }

}
