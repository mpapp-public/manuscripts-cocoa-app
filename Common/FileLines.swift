//
//  FileLines.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import os.log

/**
 Provides file lines as a sequence.
 */
struct FileLines: Sequence {

    enum LineEnd {
        case unix
        case windows
        case classicMac
        case custom([UInt8])

        var data: Data {
            switch self {
            case .unix:
                return Data([10] as [UInt8])
            case .windows:
                return Data([13, 10] as [UInt8])
            case .classicMac:
                return Data([13] as [UInt8])
            case let .custom(bytes):
                return Data(bytes)
            }
        }
    }

    let fileUrl: URL
    let lineEnd: LineEnd
    let trimLineEnd: Bool
    let lineEndData: Data
    let bufferSize: Int

    init?(fileUrl: URL, lineEnd: LineEnd = .unix, trimLineEnd: Bool = false, bufferSize: Int = 64 * 1024) {
        self.fileUrl = fileUrl
        self.lineEnd = lineEnd
        self.trimLineEnd = trimLineEnd
        self.lineEndData = lineEnd.data
        self.bufferSize = bufferSize
    }

    private func nextLine(state: IteratorState) -> Data? {
        while !state.isEndOfFile {
            if !state.currentBuffer.isEmpty {
                if let lineEndRange = state.currentBuffer.range(of: lineEndData) {
                    let line = state.currentBuffer[..<(trimLineEnd ? lineEndRange.lowerBound : lineEndRange.upperBound)]
                    if lineEndRange.upperBound < state.currentBuffer.endIndex {
                        state.currentBuffer = state.currentBuffer[lineEndRange.upperBound...]
                    } else {
                        state.currentBuffer = Data()
                    }
                    return line
                }
            }
            let nextChunk = state.fileHandle.readData(ofLength: bufferSize)
            if nextChunk.isEmpty {
                state.isEndOfFile = true
                return state.currentBuffer.isEmpty ? nil : state.currentBuffer
            }
            state.currentBuffer = Data(state.currentBuffer) + nextChunk
        }
        return nil
    }

    private class IteratorState {
        var currentBuffer = Data()
        var isEndOfFile = false
        let fileHandle: FileHandle

        init(fileHandle: FileHandle) {
            self.fileHandle = fileHandle
        }
    }

    func makeIterator() -> AnyIterator<Data> {
        do {
            let fileHandle = try FileHandle(forReadingFrom: fileUrl)
            let state = IteratorState(fileHandle: fileHandle)
            return AnyIterator<Data> {
                return self.nextLine(state: state)
            }
        } catch let error {
            os_log(.error, "can not iterate lines of file %{public}@, error=%{public}@", fileUrl.path, error.localizedDescription)
            return AnyIterator<Data> {
                return nil
            }
        }
    }
}

/**
 Sequence of lines of a file chunk.
 */
struct ChunkLines: Sequence {

    fileprivate let state: FileChunks.IteratorState

    func makeIterator() -> AnyIterator<Data> {
        return AnyIterator<Data> {
            if let firstLine = self.state.nextChunkFirstLine {
                self.state.nextChunkFirstLine = nil
                return firstLine
            } else {
                guard let nextLine = self.state.fileLinesIterator.next() else {
                    return nil
                }
                if self.state.isNextChunkLine(nextLine) {
                    self.state.nextChunkFirstLine = nextLine
                    return nil
                }
                return nextLine
            }
        }
    }
}

/**
 Sequence of file chunks as recognised by `chunkRecogniser`.

 Used to process file per parts if there are issues to process the file as whole.
 */
struct FileChunks: Sequence {

    typealias ChunkRecogniser = (Data) -> Bool

    let fileLines: FileLines
    var chunkRecogniser: ChunkRecogniser?

    fileprivate class IteratorState {
        var fileLinesIterator: FileLines.Iterator
        var nextChunkFirstLine: Data?
        var hasStarted = false
        var chunkRecogniser: ChunkRecogniser?

        init(fileLines: FileLines, chunkRecogniser: ChunkRecogniser?) {
            self.fileLinesIterator = fileLines.makeIterator()
            self.chunkRecogniser = chunkRecogniser
        }

        func isNextChunkLine(_ data: Data) -> Bool {
            guard hasStarted else {
                hasStarted = true
                return false
            }
            hasStarted = true
            return chunkRecogniser?(data) ?? false
        }
    }

    func makeIterator() -> AnyIterator<ChunkLines> {
        let state = IteratorState(fileLines: fileLines, chunkRecogniser: chunkRecogniser)
        return AnyIterator<ChunkLines> {
            if !state.hasStarted || state.nextChunkFirstLine != nil {
                return ChunkLines(state: state)
            } else {
                return nil
            }
        }
    }
}
