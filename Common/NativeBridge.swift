//
//  NativeBridge.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import os.log

/**
 Implemented by EditorWebViewCoordinator to forward native calls execution to its coordinated web view.
 */
protocol NativeBridgeProvider: class {
    func cite(citation: String, type: String)
    func importBibliography(uuid: UUID, data: Data, format: BibliographyDataFormat, completion: @escaping NativeBridgeCompletion)

    func performSyncIdle(block: @escaping () -> Void)
}

enum NativeBridgeError: LocalizedError {
    case timeout
    case javaScriptError(Error)
    case taskError(String)
}

typealias NativeBridgeResult = Result<Any?, NativeBridgeError>
typealias NativeBridgeCompletion = (NativeBridgeResult) -> Void

struct NativeBridgePendingTask {
    let timeoutTimer: DispatchSourceTimer
    let completion: NativeBridgeCompletion
}

/**
 Holds reference to the `NativeBridgeProvider` instance for WKWebView interactions.

 It is provided to SwiftUI view as an environment object to avoid a strong reference cycle.
 */
class NativeBridge: ObservableObject {

    struct OSACommandKey {
        static let bibliographyDataFormat = "BibliographyDataFormat"
        static let citationString = "CitationString"
    }

    var nativeBridgeProvider: NativeBridgeProvider?

    // MARK: Apple Script
    func cite(with command: NSScriptCommand) {
        guard
            let type = command.arguments?[OSACommandKey.bibliographyDataFormat] as? String,
            let citation = command.arguments?[OSACommandKey.citationString] as? String
        else {
            os_log(.error, "unsupported cite command arguments: %{public}@", String(describing: command.arguments))
            return
        }
        nativeBridgeProvider?.cite(citation: citation, type: type)
    }

    func importBibliography(uuid: UUID, data: Data, format: BibliographyDataFormat, completion: @escaping NativeBridgeCompletion) {
        nativeBridgeProvider?.importBibliography(uuid: uuid, data: data, format: format, completion: completion)
    }

    // MARK: - Concurrency

    /** This performs given block on the at the moment when CBL syncing is idle. */

    func performSyncIdle(block: @escaping () -> Void) {
        nativeBridgeProvider?.performSyncIdle(block: block)
    }
}
