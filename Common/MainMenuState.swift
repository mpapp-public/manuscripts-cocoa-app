//
//  MainMenuState.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import Combine
#if os(macOS)
import AppKit
#endif

#if os(macOS)
@objc class MainMenuUpdateRequest: NSObject {
    let updateIdentifier: String
    let menu: NSMenu

    init(identifier: String, menu: NSMenu) {
        self.updateIdentifier = identifier
        self.menu = menu
    }
}

@objc protocol MenuUpdateProvider: class {
    @objc func registerMenuUpdateRequest(_ request: MainMenuUpdateRequest)
    @objc optional func unregisterMenuUpdateRequest(_ request: MainMenuUpdateRequest)
}

#endif

enum MainMenuItem: String, StoredStateItem, CaseIterable {

    struct State: StoredStateValue {
        let isEnabled: Bool
        let isActive: Bool
        let title: String?

        static let defaultValue = State(isEnabled: false, isActive: false)

        init(isEnabled: Bool, isActive: Bool, title: String? = nil) {
            self.isEnabled = isEnabled
            self.isActive = isActive
            self.title = title

            #if DEBUG
            for item in MainMenuItem.allCases {
                assert(MainMenuItem.tagMapping.values.contains(item), "devel assertion failure: menu item \(item) not properly mapped")
            }
            #endif
        }
    }

    case fileExport = "export"
    case fileDeleteProject = "delete-project"
    case fileDeleteManuscript = "delete-manuscript"
    case fileRenameProject = "rename-project"

    case fileNewManuscript = "project-new-manuscript"

    //case eport

    case editUndo = "edit-undo"
    case editRedo = "edit-redo"
    case editDelete = "edit-delete"

    case insertParagraph = "insert-paragraph"
    case insertNumberedList = "insert-ordered-list"
    case insertBulletList = "insert-bullet-list"
    case insertFigurePanel = "insert-figure-element"

    case insertBlockQuote = "insert-blockquote"
    case insertPullQuote = "insert-pullquote"

    case insertTable = "insert-table-element"
    case insertListing = "insert-listing"
    case insertLink = "insert-link"
    case insertEquation = "insert-equation"
    case insertInlineEquation = "insert-inline-equation"
    case insertCitation = "insert-citation"
    case insertCrossReference = "insert-cross-reference"
    case insertKeywords = "insert-keywords"
    case insertTableOfContents = "insert-toc"
    case insertBibliography = "insert-bibliography"

    case formatBold = "format-bold"
    case formatItalic = "format-italic"
    case formatUnderline = "format-underline"
    case formatSuperscript = "format-superscript"
    case formatSubscript = "format-subscript"
    case formatStrikethrough = "format-strikethrough"
    case formatTable = "format-table"

    case tableRowAbove = "format-table-add-row-before"
    case tableRowBelow = "format-table-add-row-after"
    case tableRowDelete = "format-table-delete-row"
    case tableColumnBefore = "format-table-add-column-before"
    case tableColumnAfter = "format-table-add-column-after"
    case tableColumnDelete = "format-table-delete-column"

    static let reservedMenuTags = 100..<1000

    ///Menu tags 100...999 are reserved for WebKit related actions.
    static let tagMapping: [Int: Self] = [
        501: .fileExport,
        502: .fileDeleteProject,
        503: .fileDeleteManuscript,
        504: .fileRenameProject,

        601: .fileNewManuscript,

        101: .editUndo,
        102: .editRedo,
        103: .editDelete,

        201: .insertParagraph,
        202: .insertNumberedList,
        203: .insertBulletList,
        204: .insertFigurePanel,
        205: .insertTable,
        206: .insertListing,
        207: .insertLink,
        208: .insertEquation,
        209: .insertInlineEquation,
        210: .insertCitation,
        211: .insertCrossReference,
        212: .insertBibliography,
        213: .insertBlockQuote,
        214: .insertPullQuote,
        215: .insertKeywords,
        216: .insertTableOfContents,

        301: .formatBold,
        302: .formatItalic,
        303: .formatUnderline,
        304: .formatSuperscript,
        305: .formatSubscript,
        306: .formatStrikethrough,
        307: .formatTable,

        401: .tableRowAbove,
        402: .tableRowBelow,
        403: .tableRowDelete,
        404: .tableColumnBefore,
        405: .tableColumnAfter,
        406: .tableColumnDelete
    ]

    typealias Message = [String: [String: Any]]

    static func translate(stateArray: [[String: Any]]) -> Message {
        var translated = Message()
        for item in stateArray {
            guard let identifier = item["id"] as? String else {
                continue
            }
            #if DEBUG
            assert(MainMenuItem(rawValue: identifier) != nil, "unknown menu identifier: \(identifier)")
            #endif
            translated[identifier] = item
        }
        return translated
    }

    func itemState(from message: Message) -> MainMenuItem.State? {
        guard let messageItem = message[self.rawValue] else {
            return nil
        }
        let enabled = messageItem["enabled"] as? Bool ?? false
        let active = messageItem["active"] as? Bool ?? false
        return State(isEnabled: enabled, isActive: active)
    }
}

class MainMenuState: StateStorage<MainMenuItem> {
    let menuUpdateRequest = PassthroughSubject<String, Never>()

    func requestMenuUpdate(with identifier: String) {
        menuUpdateRequest.send(identifier)
    }

    private(set) var itemSignal = PassthroughSubject<MainMenuItem, Never>()

    func signalAction(for item: MainMenuItem) {
        itemSignal.send(item)
    }

}
