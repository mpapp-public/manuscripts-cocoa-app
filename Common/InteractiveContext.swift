//
//  InteractiveContext.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import Combine

/**
 Used as an environment object of the editor view.
 Provides context for extra user interactions - e.g. a modal sheet.
 */
class InteractiveContext: ObservableObject {
    private var taskStateSink: AnyCancellable?

    @Published var interactiveTask: InteractiveTask? {
        didSet {
            taskStateSink = watchTaskState()
        }
    }

    private func watchTaskState() -> AnyCancellable? {
        return interactiveTask?.$state
            .sink { [weak self] value in
                switch value {
                case .cancelled:
                    self?.interactiveTask = nil
                default:
                    break
                }
            }
    }
}
