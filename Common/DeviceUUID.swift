//
//  DeviceUUID.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation

#if os(iOS)
import UIKit
#endif

struct DeviceUUID {
    static var uuid: UUID {
        #if os(iOS)
        return UIDevice.current.identifierForVendor ?? UUID()
        #else
        if let uuidString = UserDefaults.standard.object(forKey: "DeviceIdentifier") as? String,
            let uuid = UUID(uuidString: uuidString) {
            return uuid
        } else {
            let uuid = UUID()
            UserDefaults.standard.set(uuid.uuidString, forKey: "DeviceIdentifier")
            UserDefaults.standard.synchronize()
            return uuid
        }
        #endif
    }
}
