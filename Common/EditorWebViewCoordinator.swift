//
//  EditorWebViewCoordinator.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import SwiftUI
import WebKit
import Combine
import os.log

#if os(macOS)
import AppKit
#endif

//This has to be NSObject to satisfy protocol requrements
class EditorWebViewCoordinator: NSObject {

    static let nativeUrlPath = "native"
    #if DEBUG
    static let developerUrlPath = "developer"
    #endif

    static let toolbarHandlerName = "toolbar"
    static let syncHandlerName = "sync"
    static let actionHandlerName = "action"
    static let taskCallbackHandlerName = "taskCallback"

    private let content: EditorContent
    private let webViewLoaded: Binding<Bool>
    private let mainMenuState: MainMenuState
    private let toolbarState: ToolbarState

    private var toolbarSignalSink: AnyCancellable?
    private var mainMenuUpdateRequestSink: AnyCancellable?
    private var mainMenuSignalSink: AnyCancellable?
    private var sessionStateSink: AnyCancellable?

    private var sessionManager: SessionManager { AppEnvironment.shared.sessionManager }

    private var collectionSyncState = [String: Bool]() {
        didSet {
            dispatchPrecondition(condition: .onQueue(.main))
            isSyncBusy = collectionSyncState.values.contains(true)
        }
    }

    @Published
    var isSyncBusy: Bool = false {
        didSet {
            dispatchPrecondition(condition: .onQueue(.main))
            if !isSyncBusy && !syncIdleBlocks.isEmpty {
                performSyncIdleBlocks()
            }
        }
    }

    init(content: EditorContent, mainMenuState: MainMenuState, toolbarState: ToolbarState, webViewLoaded: Binding<Bool>) {
        self.content = content
        self.webViewLoaded = webViewLoaded
        self.mainMenuState = mainMenuState
        self.toolbarState = toolbarState

        super.init()

        toolbarSignalSink = toolbarSignal()
        mainMenuUpdateRequestSink = mainMenuUpdate()
        mainMenuSignalSink = mainMenuSignal()
        sessionStateSink = watchSessionState()
    }

    private func toolbarSignal() -> AnyCancellable {
        return toolbarState.itemSignal.sink { [weak self] item in
            self?.webView.evaluateJavaScript("window.dispatchToolbarAction('\(item.rawValue)')") { (_, error) in
                if let err = error {
                    os_log(.debug, "error executing toolbar action: %{public}s", err.localizedDescription)
                }
            }
        }
    }

    private func mainMenuUpdate() -> AnyCancellable {
        return mainMenuState.menuUpdateRequest
            .sink { [weak self] menuIdentifier in
                self?.webView.evaluateJavaScript("window.getMenuState('\(menuIdentifier)')") { (state, error) in
                    guard error == nil else {
                        os_log("error obtaining menu stata for '%{public}@': %{public}@", menuIdentifier, error!.localizedDescription)
                        self?.mainMenuState.reset()
                        return
                    }

                    switch state {
                    //unefined or null from getMenuState
                    case nil, is NSNull:
                        self?.mainMenuState.reset()
                    //standard response
                    case let stateArray as [[String: Any]]:
                        let translatedState = MainMenuItem.translate(stateArray: stateArray)
                        self?.mainMenuState.refresh(with: translatedState)
                    default:
                        os_log(.error, "unexpected menu state format")
                        self?.mainMenuState.reset()
                    }
            }
        }
    }

    private func mainMenuSignal() -> AnyCancellable {
        return mainMenuState.itemSignal
            .sink { [weak self] item in
                self?.webView.evaluateJavaScript("window.dispatchMenuAction('\(item.rawValue)')") { (_, error) in
                    if let err = error {
                        os_log("error executing menu action: %{public}s", err.localizedDescription)
                    }
                }
        }
    }

    /// Shortcut to the content scope session state
    private var sessionState: SessionState {
        sessionManager[content.enclave.scope]
    }

    private var sessionStateUpdateSink: AnyCancellable?

    private func reflectSessionState(_ nextSessionState: SessionState) {
        let diff = nextSessionState.diff(from: sessionState)
        guard !diff.contains(.user) else {
            preconditionFailure("Can not handle user change in the webkit app, all editor windows should have been closed before the account is switched.")
        }
        guard
            sessionStateUpdateSink == nil,
            !diff.intersection([.token, .cookies]).isEmpty
        else {
            return
        }

        let cookies: [HTTPCookie] = diff.contains(.cookies) ? Array((nextSessionState.gatewayCookies ?? [:]).values) : []

        sessionStateUpdateSink = cookies.publisher
            .flatMap(self.futureSetCookie)
            .replaceEmpty(with: true)
            .last()
            .flatMap { _ in self.futureSetToken(diff.contains(.token) ? nextSessionState.token : nil) }
            .flatMap { _ in self.futureNotifySessionDiff(diff) }
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { _ in
                self.sessionStateUpdateSink = nil
            }, receiveValue: { _ in })
    }

    /**
     This method watches changes in the session state and applies eventual changes in the session info data (cookies, token).
     */
    private func watchSessionState() -> AnyCancellable? {
        return sessionManager.sessionStatePublisher(for: content.enclave.scope)
            .removeDuplicates()
            .sink { [weak self] nextSessionState in
                self?.reflectSessionState(nextSessionState)
            }
    }

    private var bootstrapNavigation: WKNavigation?

    private func futureSetCookie(_ cookie: HTTPCookie) -> Future<Bool, Never> {
        Future<Bool, Never> { promise in
            WKWebsiteDataStore.default().httpCookieStore.setCookie(cookie) {
                promise(.success(true))
            }
        }
    }

    private func futureSetToken(_ token: String?) -> Future<Bool, Never> {
        Future<Bool, Never> { promise in
            guard let realToken = token else {
                promise(.success(false))
                return
            }
            let injectTokenJS = "window.localStorage.setItem('token', '\(realToken)')"
            self.webView.evaluateJavaScript(injectTokenJS) { (_, error) in
                if let err = error {
                    os_log(.error, "error setting WebKitView token: %{public}s", err.localizedDescription)
                }
                promise(.success(true))
            }
        }
    }

    private func futureNotifySessionDiff(_ sessionDiff: SessionState.DiffResult) -> Future<Bool, Never> {
        Future<Bool, Never> { promise in
            if sessionDiff.contains(.cookies) {
                let restartSyncJS = "window.restartSync()"
                self.webView.evaluateJavaScript(restartSyncJS) { (_, error) in
                    if let err = error {
                        os_log(.error, "error setting restarting collection sync: %{public}@", err.localizedDescription)
                    }
                    promise(.success(true))
                }
            } else {
                promise(.success(true))
            }
        }
    }

    // MARK: - WebKit Delegate
    private func resourceURLSchemeHandler() -> ResourceURLSchemeHandler {
        let handler = ResourceURLSchemeHandler()
        handler.map(pathPrefix: "data", to: "manuscripts-data")
        handler.map(pathPrefix: CouchbaseLiteRESTSchemeHandler.pathPrefix, to: CouchbaseLiteRESTSchemeHandler(documentServer: content.enclave.documentServer))
        return handler
    }

    //avoid a retain cycle
    lazy var messageHandlerProxy = MessageHandlerProxy(provider: self)

    private func webKitViewConfiguration() -> WKWebViewConfiguration {
        let config = WKWebViewConfiguration()
        config.setURLSchemeHandler(resourceURLSchemeHandler(), forURLScheme: "app-resources")
        config.preferences.setValue(true, forKey: "developerExtrasEnabled")
        config.preferences.setValue(true, forKey: "allowFileAccessFromFileURLs")
        config.userContentController.add(messageHandlerProxy, name: Self.toolbarHandlerName)
        config.userContentController.add(messageHandlerProxy, name: Self.syncHandlerName)
        config.userContentController.add(messageHandlerProxy, name: Self.actionHandlerName)
        config.userContentController.add(messageHandlerProxy, name: Self.taskCallbackHandlerName)
        return config
    }

    private var webView: WKWebView!

    func ensureWebView() -> WKWebView {
        guard webView == nil else { return webView! }

        webView = WKWebView(frame: .zero, configuration: webKitViewConfiguration())
        webView.navigationDelegate = self
        webView.uiDelegate = self
        webView.allowsBackForwardNavigationGestures = true

        loadWebView()

        return webView
    }

    private var loadWebViewSink: AnyCancellable?

    private func resolveUrl(path: String) -> URL {
        return URL(string: path, relativeTo: content.enclave.scope.editorBaseUrl)!
    }

    private func loadWebView() {
        guard loadWebViewSink == nil else { return }

        var cookies = [HTTPCookie]()

        let url = resolveUrl(path: Self.nativeUrlPath)

        var comps = URLComponents(url: url, resolvingAgainstBaseURL: true)!

        //query-like fragment items
        var fragmentItems = [URLQueryItem]()

        if let token = sessionState.token {
            //add gateway cookies to cookies we need to set if we have a token
            if let gatewayCookies = sessionState.gatewayCookies?.values {
                cookies += gatewayCookies
            }
            fragmentItems.append(URLQueryItem(name: "access_token", value: token))
        }

        switch content.action {
        case .openProject:
            if let proj = content.project {
                fragmentItems.append(URLQueryItem(name: "destination", value: "/projects/\(proj.documentId)"))
            }
        case .newProject:
            fragmentItems.append(URLQueryItem(name: "destination", value: "/new-project"))
        default:
            break
        }

        if !fragmentItems.isEmpty {
            var auxComponents = URLComponents()
            auxComponents.queryItems = fragmentItems
            comps.fragment = auxComponents.query
        }

        assert(comps.url != nil, "editor url invalid after setting its fragment")

        //set cookies and load the url
        loadWebViewSink = cookies.publisher
            .flatMap(self.futureSetCookie)
            .replaceEmpty(with: true)
            .last()
            .sink(receiveCompletion: { [weak self] _ in
                self?.bootstrapNavigation = self?.webView.load(URLRequest(url: comps.url!))
                self?.loadWebViewSink = nil
            }, receiveValue: {_ in})
    }

    // MARK: - Extension Properties
    fileprivate var updateProjectSink: AnyCancellable?
    fileprivate var pendingTasks = [UUID: NativeBridgePendingTask]()
    fileprivate var pendingTasksLock = NSLock()

    fileprivate var syncIdleBlocks = [() -> Void]()
}

extension EditorWebViewCoordinator: WKScriptMessageHandler {

    enum EditorAction: String {
        case closeWindow = "close-window"
        case assignProject = "assign-project"
    }

    enum SyncAction: String {
        case refreshSession = "refresh-session"
        case setActive = "set-active"
    }

    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        switch message.name {
        case Self.toolbarHandlerName:
            guard
                let typedMessage = message.body as? [String: [String: Any]],
                let toolbarMessage = typedMessage["manuscript"]?["toolbar"] as? [String: [String: Any]]
            else {
                os_log(.error, "invalid webkit toolbar message")
                return
            }
            toolbarState.refresh(with: toolbarMessage)
        case Self.syncHandlerName:
            processSyncMessage(message)
        case Self.actionHandlerName:
            processActionMessage(message)
        case Self.taskCallbackHandlerName:
            processTaskCallback(message)
        default:
            break
        }
    }

    private func updateContentProject(_ identifier: String) {
        updateProjectSink = content.enclave.documentServer.projectList.$projects
            .compactMap { projects -> Project? in
                return projects[identifier]
            }
            .timeout(5, scheduler: RunLoop.main)
            .sink(receiveCompletion: { [weak self] completion in
                guard let this = self else { return }
                if case .finished = completion {
                    assertionFailure("could not resolve a new porject in time")
                }
                this.updateProjectSink = nil
            }, receiveValue: { [weak self] project in
                guard let this = self else { return }
                this.content.takeProject(project)
                this.updateProjectSink = nil
            })
    }

    private func processTaskCallback(_ message: WKScriptMessage) {
        guard
            let typedMessage = message.body as? [String: Any],
            let uuidString = typedMessage["batchId"] as? String,
            let uuid = UUID(uuidString: uuidString)
        else {
            os_log(.error, "invalid task callback")
            return
        }
        if let errorMessage = typedMessage["error"] as? String {
            completeTask(uuid: uuid, result: .failure(NativeBridgeError.taskError(errorMessage)))
        } else {
            completeTask(uuid: uuid, result: .success(typedMessage))
        }
    }

    private func processSyncMessage(_ message: WKScriptMessage) {
        guard
            let typedMessage = message.body as? [String: Any],
            let actionName = typedMessage["name"] as? String,
            let action = SyncAction(rawValue: actionName)
        else {
            os_log(.error, "invalid sync webkit message")
            return
        }
        switch action {
        case .refreshSession:
            assert(content.enclave.scope == .cloud)
            sessionManager.refreshSyncGatewayCookies()
        case .setActive:
            guard
                let isActive = typedMessage["active"] as? Bool,
                let collectionName = typedMessage["collection"] as? String
            else {
                preconditionFailure("invalid sync message")
            }
            if collectionName == "user" || collectionName.hasPrefix("project_mpproject_") {
                collectionSyncState[collectionName] = isActive
            }
        }
    }

    private func processActionMessage(_ message: WKScriptMessage) {
        guard
            let typedMessage = message.body as? [String: Any],
            let actionName = typedMessage["name"] as? String,
            let action = EditorAction(rawValue: actionName)
        else {
            os_log(.error, "invalid action webkit message")
            return
        }
        switch action {
        case .closeWindow:
            #if os(iOS)
            #error("unimplemented")
            #else
            (NSDocumentController.shared as? DocumentController)?.closeDocument(with: content)
            #endif
        case .assignProject:
            guard let projectId = typedMessage["projectID"] as? String else {
                os_log(.error, "no ID for the assigned project")
                return
            }
            updateContentProject(projectId)
        }
    }
}

// MARK: - Navigation Delegate
extension EditorWebViewCoordinator: WKNavigationDelegate {

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        switch navigation {
        case bootstrapNavigation:
            webViewLoaded.wrappedValue = true
        default:
            break
        }
    }

    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        os_log(.error, "WKWebView failed to load %{public}s with error %{public}s", String(describing: navigation), error.localizedDescription)
    }

    func webView(_ webView: WKWebView,
                 decidePolicyFor navigationAction: WKNavigationAction,
                 decisionHandler: @escaping (WKNavigationActionPolicy) -> Swift.Void) {
        os_log(.debug, "Deciding navigation action for: %{public}s", navigationAction.debugDescription)

        guard let url = navigationAction.request.url else {
            os_log(.error,
                   "Cancelling navigation action with no URL: %{public}s", navigationAction.debugDescription)
            decisionHandler(.cancel)
            return
        }
        guard let verb = navigationAction.request.httpMethod else {
            os_log(.error,
                   "Cancelling navigation action with no HTTP method: %{public}s", navigationAction.debugDescription)
            decisionHandler(.cancel)
            return
        }
        os_log(.debug,
               "Allowing navigation action: %{public}s", ["method": verb, "url": url].debugDescription)
        decisionHandler(.allow)
    }
}

// MARK: - Native Pridge Provider
extension EditorWebViewCoordinator: NativeBridgeProvider {

    // MARK: - Bibliography
    func cite(citation: String, type: String) {
        guard let encodedCitation = citation.data(using: .utf8)?.base64EncodedString() else {
            return
        }
        let javaScript = "window.dispatchCitation('\(encodedCitation)', '\(type)', true)"

        executeTask(script: javaScript, uuid: UUID()) { result in
            if case let .failure(error) = result {
                os_log(.error, "error instering citation: %{public}", error.localizedDescription)
            }
        }
    }

    private func updatePendingTasks(block: () -> Void) {
        pendingTasksLock.lock()
        defer { pendingTasksLock.unlock() }
        block()
    }

    private func completeTask(uuid: UUID, result: NativeBridgeResult) {
        updatePendingTasks {
            guard let pending = pendingTasks[uuid] else { return }

            pendingTasks[uuid] = nil
            pending.completion(result)
        }
    }

    private func executeTask(script: String, uuid: UUID, timeout: DispatchTimeInterval = .seconds(900), completion: @escaping NativeBridgeCompletion) {
        dispatchPrecondition(condition: .onQueue(.main))

        updatePendingTasks {
            let timer = DispatchSource.makeTimerSource(flags: [], queue: DispatchQueue.main)
            timer.schedule(deadline: .now() + timeout, repeating: .never)
            timer.setEventHandler { [weak self] in
                self?.completeTask(uuid: uuid, result: .failure(NativeBridgeError.timeout))
            }
            timer.resume()
            let pending = NativeBridgePendingTask(timeoutTimer: timer, completion: completion)
            pendingTasks[uuid] = pending
        }

        webView.evaluateJavaScript(script) { (_, error) in
            if let realError = error {
                self.completeTask(uuid: uuid, result: .failure(NativeBridgeError.javaScriptError(realError)))
            }
        }
    }

    func importBibliography(uuid: UUID, data: Data, format: BibliographyDataFormat, completion: @escaping NativeBridgeCompletion) {
        let encodedData = data.base64EncodedString()
        let javaScript = "window.dispatchCitation('\(encodedData)', '\(format.contentType)', false, '\(uuid.uuidString)')"

        executeTask(script: javaScript, uuid: uuid, completion: completion)
    }

    func performSyncIdle(block: @escaping () -> Void) {
        dispatchPrecondition(condition: .onQueue(.main))
        syncIdleBlocks.append(block)
        if !isSyncBusy {
            performSyncIdleBlocks()
        }
    }

    fileprivate func performSyncIdleBlocks() {
        dispatchPrecondition(condition: .onQueue(.main))

        //executing blocks may schedule another ones
        let capturedBlocks = syncIdleBlocks
        syncIdleBlocks.removeAll()

        for block in capturedBlocks {
            block()
        }
    }

}

extension EditorWebViewCoordinator: WKUIDelegate {

    #if os(macOS)
    func webView(_ webView: WKWebView, runOpenPanelWith parameters: WKOpenPanelParameters, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping ([URL]?) -> Void) {

        guard let window = webView.window else {
            completionHandler(nil)
            return
        }

        let panel = NSOpenPanel()
        panel.title = "Select Image"
        panel.allowsMultipleSelection = parameters.allowsMultipleSelection
        panel.canChooseDirectories = parameters.allowsDirectories
        panel.allowedFileTypes = ["public.image"]

        panel.beginSheetModal(for: window) { (response) in
            guard response == .OK else {
                completionHandler(nil)
                return
            }
            completionHandler(panel.urls)
        }
    }
    #endif

}
