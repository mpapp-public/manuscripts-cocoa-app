//
//  ProjectManagerView.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import SwiftUI

struct ProjectDeleteSheet: View {

    let project: Project

    @Environment(\.presentationMode) var presentationMode
    @State private var text: String = ""

    @State private var error: Error?

    var body: some View {
        VStack(alignment: .leading) {
            Text("Delete Project")
                .font(.headline)
            Text("Are you sure you wish to delete the project with title \"\(self.project.displayTitle)\"?")
            if error != nil {
                Text(error!.localizedDescription)
                    .lineLimit(3)
                    .foregroundColor(Color.red)
                    .transition(.opacity)
                    .padding(.vertical, 10)
            }
            TextField("Please type: DELETE", text: $text)
            HStack {
                Spacer()
                Button(action: {self.presentationMode.wrappedValue.dismiss()}, label: {
                    Text("Cancel")
                })
                Button(action: {self.deleteProject()}, label: {
                    Text("Delete")
                })
                .disabled(self.text != "DELETE")
            }
        }
        .padding()
        .frame(width: 250)
    }

    private func deleteProject() {
        project.closeDocument { (closed) in
            guard closed else { return }
            self.project.delete { (error) in
                DispatchQueue.main.async {
                    guard error == nil else {
                        withAnimation { self.error = error }
                        return
                    }
                    self.presentationMode.wrappedValue.dismiss()
                }
            }
        }
    }
}

struct ProjectRenameSheet: View {
   let project: Project

    @Environment(\.presentationMode) var presentationMode
    @State private var text: String

    init(project: Project) {
        self.project = project
        _text = State(initialValue: project.title ?? "")
    }

    @State private var error: Error?

    var body: some View {
        VStack(alignment: .leading) {
            Text("Rename Project")
                .font(.headline)
            if error != nil {
                Text(error!.localizedDescription)
                    .lineLimit(3)
                    .foregroundColor(Color.red)
                    .transition(.opacity)
                    .padding(.vertical, 10)
            }
            TextField("Untitled Project", text: $text)
            HStack {
                Spacer()
                Button(action: {self.presentationMode.wrappedValue.dismiss()}, label: {
                    Text("Cancel")
                })
                Button(action: {self.renameProject()}, label: {
                    Text("Rename")
                })
            }
        }
        .padding()
        .frame(width: 250)
    }

    private func renameProject() {
        project.rename(to: text) { (error) in
            DispatchQueue.main.async {
                guard error == nil else {
                    withAnimation { self.error = error }
                    return
                }
                self.presentationMode.wrappedValue.dismiss()
            }
        }
    }
}

/// View representing a single project
struct ProjectListCell: View {

    enum ProjectAction: Int, Identifiable {
        case rename
        case delete

        //swiftlint:disable identifier_name
        var id: Int { return rawValue }
    }

    class CellState: ObservableObject, Selectable {
        let project: Project

        @Published var isSelected = false

        init(project: Project) {
            self.project = project
        }
    }

    let listSelection: Selection<CellState>
    @ObservedObject var state: CellState
    @ObservedObject var project: Project

    @State private var projectAction: ProjectAction?

    init(state: CellState, listSelection: Selection<CellState>) {
        self.state = state
        self.listSelection = listSelection
        self.project = state.project
    }

    var body: some View {
        VStack(alignment: .leading, spacing: 0) {
            HStack(spacing: 0) {
                Text(project.displayTitle)
                    .font(.system(size: 18))
                Spacer()
                MenuButton(label: Text("...")) {
                        Button(action: {self.openProject()}, label: {Text("Open")})
                    Button(action: {self.projectAction = .rename}, label: {Text("Rename...")})
                        //Divider() //does not work as expected for now
                    Button(action: {self.projectAction = .delete}, label: {Text("Delete...")})
                }
                .menuButtonStyle(BorderlessButtonMenuButtonStyle())
                .frame(width: 30, alignment: .trailing)
                //known multiple modals issue, see e.g.:
                //https://stackoverflow.com/questions/57103800/swiftui-support-multiple-modals/57105822
                .sheet(item: $projectAction) { action in
                    if action == .rename {
                        ProjectRenameSheet(project: self.state.project)
                    }
                    if action == .delete {
                        ProjectDeleteSheet(project: self.state.project)
                    }
                }
            }
            .padding()
            Divider()
                .padding(.horizontal)
        }
        .background(state.isSelected ? Color(white: 0.9) : Color.white)
        .onTapGesture {
            self.listSelection.selectedItem = self.state
            self.openProject()
        }
    }

    private func openProject() {
        let content = EditorContent(project: self.project)
        _ = try? (NSDocumentController.shared as? DocumentController)?.open(content: content)
    }
}

/// List of projects with basic controls for project mnagement.
struct ProjectListView: View {

    @ObservedObject var projectList: ProjectList
    let selection = Selection<ProjectListCell.CellState>()

    func newProject() {
        guard let enclave = projectList.enclave else { return }
        let content = EditorContent(enclave: enclave, action: .newProject)
        _ = try? (DocumentController.shared as? DocumentController)?.open(content: content)
    }

    var body: some View {
        let listData: [ProjectListCell.CellState] = projectList.projects.values
            .sorted(by: {$0.displayTitle < $1.displayTitle})
            .map {
                ProjectListCell.CellState(project: $0)
             }

        return VStack(alignment: .leading, spacing: 0) {
            HStack {
                Button(action: {self.newProject()}, label: {
                    Text("New Project")
                })
                .foregroundColor(.blue)
                .font(.system(size: 16))
                .buttonStyle(PlainButtonStyle())
                .padding()
                Spacer()
            }
            //scale to full estate even if empty
            GeometryReader { geo in
                ScrollView(.vertical, showsIndicators: true) {
                    VStack(spacing: 0) {
                        ForEach(listData, id: \.project.documentId) { state in
                            ProjectListCell(state: state, listSelection: self.selection)
                        }
                    }
                }
                .frame(width: geo.size.width, height: geo.size.height)
                .background(Color.white)
            }
        }
    }
}

/// View allowing the user to log in to see .cloud projects.
struct CloudProjectsLoginView: View {
    @ObservedObject var sessionManager = AppEnvironment.shared.sessionManager

    var body: some View {
        VStack {
            Spacer()
            Text("You need to log in to access your cloud documents.")
                .multilineTextAlignment(.center)
                .font(.system(size: 16))
                .padding()
            Spacer()
            HStack {
                Spacer()
                FlatButton(title: "Log in", color: .buttonBlue, invertedColor: .white) {
                    self.sessionManager.requestLogin()
                }
                .frame(width: 200, height: 48)
                Spacer()
            }
            Spacer()
        }
    }
}

/// Container for .cloud scope, offers login if the user is not logged in.
struct CloudProjectsView: View {
    @ObservedObject var sessionManager = AppEnvironment.shared.sessionManager
    @ObservedObject var dataManager = AppEnvironment.shared.dataManager

    @State var needsLogin: Bool?

    var body: some View {
        let showLoginView = needsLogin ?? (sessionManager.cloudSessionState.user == nil)

        return ZStack {
            if showLoginView {
                CloudProjectsLoginView()
                    .transition(.opacity)
            } else {
                ProjectListView(projectList: dataManager[.cloud].documentServer.projectList)
                    .transition(.opacity)
            }
        }
        .onReceive(sessionManager.$cloudSessionState) { (state) in
            withAnimation {
                self.needsLogin = state.user == nil
            }
        }
    }
}

/// Main project manager view embedded in the hosting view.
struct ProjectManagerView: View {
    @ObservedObject var dataManager = AppEnvironment.shared.dataManager

    @State private var selectedScope = DataEnclave.Scope.local

    var body: some View {
        VStack(alignment: .center, spacing: 0) {
            HStack {
                Text("Projects")
                    .font(.title)
                    .padding()
                Spacer()
            }
            HStack {
                Picker(selection: $selectedScope.animation(.default), label: EmptyView()) {
                    Text("Local").tag(DataEnclave.Scope.local)
                    Text("Cloud").tag(DataEnclave.Scope.cloud)
                }
                .pickerStyle(SegmentedPickerStyle())
                .padding(.horizontal)
            }
            if selectedScope == .local {
                ProjectListView(projectList: dataManager[.local].documentServer.projectList)
                    .tag(DataEnclave.Scope.local)
                    .transition(.asymmetric(insertion: .move(edge: .leading), removal: .move(edge: .trailing)))
            } else {
                CloudProjectsView()
                    .tag(DataEnclave.Scope.cloud)
                    .transition(.asymmetric(insertion: .move(edge: .trailing), removal: .move(edge: .leading)))
            }
        }
        .frame(minWidth: 350, minHeight: 500)
    }
}

struct ProjectManagerViewPreviews: PreviewProvider {
    static var previews: some View {
        return ProjectManagerView()
            .frame(width: 300, height: 500, alignment: .center)
    }
}
