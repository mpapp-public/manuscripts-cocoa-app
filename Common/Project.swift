//
//  Project.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import os.log

#if os(macOS)
import AppKit
#endif

/**
 Observable object mapped to CBL _MPProject_ instances.

 Projects are typically maintained by a ProjectList instance via a live query.
 */
class Project: ObservableObject, ManuscriptsObject {
    let objectType: String
    let documentId: String
    let enclave: DataEnclave

    var isStandalone: Bool {
        enclave.scope == .standalone
    }

    @Published private(set) var title: String?

    /**
     Some OSA clients require an existing file to be provided as a ducument URL.
     We use CBL database file corresponding to the project collection.
     */
    var url: URL {
        let identifierString = documentId
            .lowercased()
            .replacingOccurrences(of: "-", with: "_")
            .replacingOccurrences(of: ":", with: "_")
        let fileName = "project_\(identifierString).cblite2"
        return enclave.dataDirectoryUrl.appendingPathComponent(fileName)
    }

    var displayTitle: String {
        (title ?? "").isEmpty ? "Untitled Project" : title!
    }

    init?(documentId: String, dict: [String: Any], enclave: DataEnclave) {
        guard let objectType = dict["objectType"] as? String else {
            return nil
        }
        self.documentId = documentId
        self.objectType = objectType
        self.title = dict["title"] as? String
        self.enclave = enclave
    }

    func update(from dict: [String: Any]) {
        self.title = dict["title"] as? String
    }

    func delete(completion: ((Error?) -> Void)?) {
        enclave.documentServer.deleteProject(documentId: documentId) { error in
            completion?(error)
        }
    }

    func rename(to newName: String, completion: ((Error?) -> Void)?) {
        enclave.documentServer.renameProject(documentId: documentId, to: newName) { error in
            completion?(error)
        }
    }

    private var closeDocumentCompletion: ((Bool) -> Void)?

    func closeDocument(completion: @escaping (Bool) -> Void) {
        guard closeDocumentCompletion == nil else {
            os_log(.info, "close document re-entered")
            return
        }

        #if os(macOS)
        guard let document = DocumentController.shared.document(for: url) else {
            completion(true)
            return
        }
        closeDocumentCompletion = completion
        document.canClose(withDelegate: self, shouldClose: #selector(document(_:shouldClose:contextInfo:)), contextInfo: CloseDocumentContext.common)
        #else
        #error("not implemented")
        #endif
    }

    #if os(macOS)
    struct CloseDocumentContext {
        private static var commonContext = 1
        @RawContext(&Self.commonContext) static var common
    }

    @objc
    private func document(_ document: NSDocument, shouldClose: Bool, contextInfo: UnsafeMutableRawPointer?) {
        if contextInfo == CloseDocumentContext.common {
            guard let completion = closeDocumentCompletion else {
                assertionFailure("documentShouldClose called without a completion")
                return
            }
            closeDocumentCompletion = nil
            if shouldClose {
                if let document = DocumentController.shared.document(for: url) {
                    document.close()
                } else {
                    os_log(.info, "closing document disappeared")
                }
            }
            completion(shouldClose)
        }
    }
    #endif
}
