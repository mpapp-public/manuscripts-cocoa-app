#!/bin/bash
set -e
DIR=$(dirname "$0")
ENVIRONMENT=${1-Development} 
export env $(grep -v '^#' $DIR/../Configuration/${ENVIRONMENT}/manuscripts-frontend.env | xargs)
cd $DIR/../External/manuscripts-frontend
yarn install
yarn build
