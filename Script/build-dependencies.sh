#!/bin/bash

set -e
DIR=$(dirname "$0")
$DIR/build-data.sh
$DIR/build-frontend.sh
