//
//  BibliographyImporterTests.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import XCTest
import Combine
@testable import Manuscripts_macOS

let standardItems = ["just_a_string".data(using: .utf8)!]
let standardResult = BibliogaphyImportResult(format: .citeprocJson, importedItems: standardItems, importErrors: [])

enum TestError: RetryableError {
    case generic

    var isRetryable: Bool { true }
}

class TestImportBase: BibliographyImport {

    func importLibrary(options: BibliographyImportOptions, completion: @escaping BibliographyImportCompletion) {
        preconditionFailure("needs to be overriden")
    }

    func importLibrary(progress: Progress?, options: BibliographyImportOptions, completion: @escaping BibliographyImportCompletion) {
        self.importLibrary(options: options, completion: completion)
    }
}

class TestAlwaysSucceedImport: TestImportBase {
    override func importLibrary(options: BibliographyImportOptions, completion: @escaping BibliographyImportCompletion) {
        DispatchQueue.global(qos: .userInitiated).async {
            completion(.success(standardResult))
        }
    }
}

class TestAlwaysFailImport: TestImportBase {
    override func importLibrary(options: BibliographyImportOptions, completion: @escaping BibliographyImportCompletion) {
        DispatchQueue.global(qos: .userInitiated).async {
            completion(.failure(BibliographyImportError.fatalFailure("failed")))
        }
    }
}

class TestAlwaysTimeoutImport: TestImportBase {
    override func importLibrary(options: BibliographyImportOptions, completion: @escaping BibliographyImportCompletion) {
        //never complete
    }
}

class TestRetryImport: TestImportBase {
    var failCount: Int

    init(failCount: Int) {
        self.failCount = failCount
    }

    override func importLibrary(options: BibliographyImportOptions, completion: @escaping BibliographyImportCompletion) {
        DispatchQueue.global(qos: .userInitiated).async {
            if self.failCount > 0 {
                self.failCount -= 1
                completion(.failure(BibliographyImportError.wrapErrorConditionally(TestError.generic)))
            } else {
                completion(.success(standardResult))
            }
        }
    }
}

class BibliographyImporterTests: XCTestCase {

    var sinks = Set<AnyCancellable>()

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testSucceed() {
        let importer = TestAlwaysSucceedImport()

        let expect = expectation(description: "expect async import")

        importer.importLibraryPublisher(timeout: 5)
            .sink(receiveCompletion: { completion in
                if case let .failure(error) = completion {
                    XCTFail("got an unexpected error: \(error.localizedDescription)")
                }
            }, receiveValue: { result in
                XCTAssertEqual(result.importedItems, standardItems)
                expect.fulfill()
            })
            .store(in: &sinks)

        waitForExpectations(timeout: 100) { (error) in
            XCTAssertNil(error, "test timeout should not happen")
        }
    }

    func testFail() {
        let importer = TestAlwaysFailImport()

        let expect = expectation(description: "expect async import")

        importer.importLibraryPublisher(timeout: 5)
            .sink(receiveCompletion: { completion in
                if case let .failure(error) = completion {
                    if case .fatalFailure = error {
                        //noop
                    } else {
                        XCTFail("unexpected error: \(error.localizedDescription)")
                    }
                    expect.fulfill()
                }
            }, receiveValue: { _ in
                XCTFail("received unexpected value")
                expect.fulfill()
            })
            .store(in: &sinks)

        waitForExpectations(timeout: 100) { (error) in
            XCTAssertNil(error, "test timeout should not happen")
        }
    }

    func testTimeout() {
        let importer = TestAlwaysTimeoutImport()

        let expect = expectation(description: "expect import timeout")

        importer.importLibraryPublisher(timeout: 1)
            .sink(receiveCompletion: { completion in
                switch completion {
                case let .failure(error):
                    if case .timeout = error {
                        //noop
                    } else {
                        XCTFail("unexpected error: \(error)")
                    }
                    expect.fulfill()
                case .finished:
                    break
                }
            }, receiveValue: { _ in
                XCTFail("got an unexpected result")
                expect.fulfill()
            })
            .store(in: &sinks)

        waitForExpectations(timeout: 10) { (error) in
            XCTAssertNil(error, "test timeout should not happen")
        }

    }

    func testRetry() {
        let importer = TestRetryImport(failCount: 3)

        let expect = expectation(description: "expect async import")

        importer.importLibraryPublisher(retries: 3)
            .sink(receiveCompletion: { completion in
                if case let .failure(error) = completion {
                    XCTFail("got an unexpected error: \(error.localizedDescription)")
                    expect.fulfill()
                }
            }, receiveValue: { importResult in
                XCTAssertEqual(importResult.importedItems, standardItems)
                XCTAssertEqual(importer.failCount, 0)
                expect.fulfill()
            })
            .store(in: &sinks)

        waitForExpectations(timeout: 100) { (error) in
            XCTAssertNil(error, "test timeout should not happen")
        }
    }

    func testTooManyRetries() {
        let importer = TestRetryImport(failCount: 5)

        let expect = expectation(description: "expect async import")

        importer.importLibraryPublisher(retries: 3)
            .sink(receiveCompletion: { completion in
                switch completion {
                case let .failure(error):
                    if case .retryable = error {
                        XCTAssertEqual(importer.failCount, 1)
                        expect.fulfill()
                    } else {
                        XCTFail("unexpected error: \(error)")
                        expect.fulfill()
                    }
                case .finished:
                    break
                }
            }, receiveValue: { _ in
                XCTFail("unexpected value")
                expect.fulfill()
            })
            .store(in: &sinks)

        waitForExpectations(timeout: 100) { (error) in
            XCTAssertNil(error, "test timeout should not happen")
        }
    }

    func testNoRetries() {
        let importer = TestRetryImport(failCount: 5)

        let expect = XCTestExpectation(description: "wait for import")

        importer.importLibraryPublisher(retries: 0) //zero retries
            .sink(receiveCompletion: { completion in
                switch completion {
                case let .failure(error):
                    if case .retryable = error {
                        XCTAssertEqual(importer.failCount, 4)
                        expect.fulfill()
                    } else {
                        XCTFail("unexpected error: \(error)")
                        expect.fulfill()
                    }
                case .finished:
                    break
                }
            }, receiveValue: { _ in
                XCTFail("unexpected value")
                expect.fulfill()
            })
            .store(in: &sinks)

        wait(for: [expect], timeout: 10)
    }

}
