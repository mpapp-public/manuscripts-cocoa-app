//
//  BookendsImportPerformance.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
import XCTest
@testable import Manuscripts_macOS

class BookendsImportPerformance: XCTestCase {

   func testImporterSmallFile() {
        let importer = BookendsBibliographyImport()

        guard let fileUrl = Bundle(for: Self.self).url(forResource: "sample", withExtension: "ris") else {
            XCTFail("file not found")
            return
        }

        measure {
            let expect = expectation(description: "expect import")
            importer.importLibrary(from: fileUrl, options: []) { _ in
                expect.fulfill()
            }

            waitForExpectations(timeout: 1000)
        }
    }

    func testImporterSmallFileChunked() {
        let importer = BookendsBibliographyImport()

        guard let fileUrl = Bundle(for: Self.self).url(forResource: "sample", withExtension: "ris") else {
            XCTFail("file not found")
            return
        }

        measure {
            let expect = expectation(description: "expect import")
            importer.importLibrary(from: fileUrl, options: [.safeMode]) { _ in
                expect.fulfill()
            }

            waitForExpectations(timeout: 1000)
        }
    }

    func testImporterBigFile() {
        let importer = BookendsBibliographyImport()

        guard let fileUrl = Bundle(for: Self.self).url(forResource: "sample-big", withExtension: "ris") else {
            XCTFail("file not found")
            return
        }

        measure {
            let expect = expectation(description: "expect import")
            importer.importLibrary(from: fileUrl, options: []) { _ in
                expect.fulfill()
            }

            waitForExpectations(timeout: 1000)
        }
    }

    func testImporterBigFileChunked() {
        let importer = BookendsBibliographyImport()

        guard let fileUrl = Bundle(for: Self.self).url(forResource: "sample-big", withExtension: "ris") else {
            XCTFail("file not found")
            return
        }

        measure {
            let expect = expectation(description: "expect import")
            importer.importLibrary(from: fileUrl, options: [.safeMode]) { _ in
                expect.fulfill()
            }

            waitForExpectations(timeout: 1000)
        }
    }
}
