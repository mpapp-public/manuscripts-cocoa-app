//
//  FilterProcessTests.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import XCTest
@testable import Manuscripts_macOS

class FilterProcessTests: XCTestCase {

    override func setUp() {
        continueAfterFailure = false
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func writeTempFile(string: String) -> URL? {
        do {
            let url = FileManager.default.temporaryDirectory.appendingPathComponent(UUID().uuidString).appendingPathExtension("txt")
            try string.write(to: url, atomically: true, encoding: .utf8)
            return url
        } catch {
            return nil
        }
    }

    private func performLineReaderTest(string: String, lineEnd: FileLines.LineEnd = .unix, expectedResult: [String]) {
        //write aux file
        let fileUrl = writeTempFile(string: string)
        XCTAssertNotNil(fileUrl, "can not write auxiliary file")
        defer { try? FileManager.default.removeItem(at: fileUrl!) }
        //create FileLines instance
        let fileLines = FileLines(fileUrl: fileUrl!, lineEnd: lineEnd, trimLineEnd: true)
        XCTAssertNotNil(fileLines, "can not open file for reading")
        //colect result to an array
        let collected = fileLines!.map({String(data: $0, encoding: .utf8)!})
        //let collected = Array(fileLines!)
        //compare
        XCTAssertEqual(collected, expectedResult)
    }

    func testLineReader() {
        let lines = ["one", "two", "three"]
        let linesWithEmpty = ["one", "", "three"]
        var fileString: String

        //no newline at the end
        fileString = lines.joined(separator: "\n")
        performLineReaderTest(string: fileString, expectedResult: lines)

        //newline at the end
        fileString += "\n"
        performLineReaderTest(string: fileString, expectedResult: lines)

        //with an empty line
        fileString = linesWithEmpty.joined(separator: "\n")
        performLineReaderTest(string: fileString, expectedResult: linesWithEmpty)

        //with CFLF (not last line)
        fileString = lines.joined(separator: "\r\n")
        performLineReaderTest(string: fileString, lineEnd: .windows, expectedResult: lines)

        //with CFLF (all)
        fileString += "\r\n"
        performLineReaderTest(string: fileString, lineEnd: .windows, expectedResult: lines)

        //CRLF with an empty line
        fileString = linesWithEmpty.joined(separator: "\r\n")
        performLineReaderTest(string: fileString, lineEnd: .windows, expectedResult: linesWithEmpty)

        //weird empty lines
        fileString = "\n\n\n\n\n"
        performLineReaderTest(string: fileString, expectedResult: [String](repeating: "", count: 5))
    }

    func testCatNoFilter() {
        let lines = ["one", "two", "three"]
        let fileString = lines.joined(separator: "\n")
        let fileUrl = writeTempFile(string: fileString)
        XCTAssertNotNil(fileUrl, "can not write auxiliary file")

        let expect = expectation(description: "perform filter")
        let filter = FilterProcess(executableUrl: URL(fileURLWithPath: "/bin/cat"), arguments: nil)

        filter.process(fileUrl: fileUrl!) { result in
            DispatchQueue.main.async {
                expect.fulfill()
                switch result {
                case let .success(chunks):
                    guard let firstChunkResult = chunks.first else {
                        XCTFail("no data from the filter")
                        return
                    }
                    switch firstChunkResult {
                    case let .success(data):
                        let string = String(data: data, encoding: .utf8)
                        XCTAssertEqual(string, fileString)
                    case let .failure(error):
                        XCTFail("unexpected error: \(error.localizedDescription)")
                    }
                case let .failure(error):
                    XCTFail("unexpected error: \(error.localizedDescription)")
                }
            }
        }
        waitForExpectations(timeout: 10, handler: nil)
    }

    func testCatFilter() {
        let lines = ["one", "two", "three"]
        let fileString = lines.joined(separator: "\n")
        let fileUrl = writeTempFile(string: fileString)
        XCTAssertNotNil(fileUrl, "can not write auxiliary file")

        let expect = expectation(description: "perform filter")
        let filter = FilterProcess(executableUrl: URL(fileURLWithPath: "/bin/cat"), arguments: nil)
        filter.linePreprocessor = { line in
            return ("filtered:" + String(data: line, encoding: .utf8)!).data(using: .utf8)!
        }

        let expectedResult = lines.map({"filtered:\($0)"}).joined(separator: "\n")

        filter.process(fileUrl: fileUrl!) { result in
            DispatchQueue.main.async {
                expect.fulfill()
                switch result {
                case let .success(chunks):
                    guard let firstChunkResult = chunks.first else {
                        XCTFail("no data from the filter")
                        return
                    }
                    switch firstChunkResult {
                    case let .success(data):
                        let string = String(data: data, encoding: .utf8)
                        XCTAssertEqual(string, expectedResult)
                    case let .failure(error):
                        XCTFail("unexpected error: \(error.localizedDescription)")
                    }
                case let .failure(error):
                    XCTFail("unexpected error: \(error.localizedDescription)")
                }
            }
        }
        waitForExpectations(timeout: 10, handler: nil)
    }

    func testSingleChunkMeasure() {
        guard let fileUrl = Bundle(for: Self.self).url(forResource: "sample-big", withExtension: "ris") else {
            XCTFail("file not found")
            return
        }

        let fileLines = FileLines(fileUrl: fileUrl, lineEnd: .classicMac)!
        let chunks = FileChunks(fileLines: fileLines, chunkRecogniser: nil)

        measure {
            var chunkCount = 0
            var lineCount = 0

            for chunk in chunks {
                chunkCount += 1
                for _ in chunk {
                    lineCount += 1
                }
            }
            XCTAssertEqual(chunkCount, 1)
            XCTAssertEqual(lineCount, 3240)
        }
    }

    static let risChunkMarker = "TY  -".data(using: .utf8)!

    func testMultipleChunksMeasure() {
        guard let fileUrl = Bundle(for: Self.self).url(forResource: "sample-big", withExtension: "ris") else {
            XCTFail("file not found")
            return
        }

        let fileLines = FileLines(fileUrl: fileUrl, lineEnd: .classicMac)!
        let chunks = FileChunks(fileLines: fileLines) { data in
            return data.prefix(Self.risChunkMarker.count) == Self.risChunkMarker
        }

        measure {
            var chunkCount = 0
            var lineCount = 0

            for chunk in chunks {
                chunkCount += 1
                for _ in chunk {
                    lineCount += 1
                }
            }
            XCTAssertEqual(chunkCount, 200)
            XCTAssertEqual(lineCount, 3240)
        }
    }

}
