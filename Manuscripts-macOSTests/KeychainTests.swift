//
//  KeychainTests.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import XCTest
@testable import Manuscripts_macOS

class KeychainTests: XCTestCase {

    override func setUp() {
        super.setUp()

        continueAfterFailure = false
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testPasswords() {
        let account = "account"
        let service = "service"
        let value1 = "password1"
        let value2 = "password2"

        do {
            //just reset before test
            try Keychain.deletePassword(account: account, service: service)

            //et initial value
            try Keychain.storePassword(value1, account: account, service: service)

            //test initial value
            let stored1 = try Keychain.getPassword(account: account, service: service)
            XCTAssertNotNil(stored1, "can not retrieve stored item")
            XCTAssertEqual(stored1, value1)

            //override initial value
            try Keychain.storePassword(value2, account: account, service: service)

            //test overriden value
            let stored2 = try Keychain.getPassword(account: account, service: service)
            XCTAssertNotNil(stored2)
            XCTAssertEqual(stored2, value2)

            //fail to update value
            do {
                try Keychain.storePassword(value1, account: account, service: service, replaceExisting: false)
                XCTFail("An error should have been thrown above")
            } catch Keychain.KeychainError.itemExists {
                //this is fine
            }

            //delete value
            try Keychain.deletePassword(account: account, service: service)
            //test value is deleted
            let stored3 = try Keychain.getPassword(account: account, service: service)
            XCTAssertNil(stored3, "failed to delete password item")

            let nilPassword = try Keychain.getPassword(account: account, service: service)
            XCTAssertNil(nilPassword)
        } catch let err as Keychain.KeychainError {
            XCTFail("keychain error \(err)")
        } catch let err {
            XCTFail("unknown error \(err)")
        }
    }

}
