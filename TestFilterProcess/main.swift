//
//  EditorWebViewMac.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation

var enterRunLoop = true

if let codeString = UserDefaults.standard.string(forKey: "exitWithCode") {
    DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
        var exitCode = Int32(codeString) ?? 100
        if UserDefaults.standard.bool(forKey: "negative") {
            exitCode = -exitCode
        }
        exit(exitCode)
    }
} else if UserDefaults.standard.bool(forKey: "terminateBySignal") {
    DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
        kill(getpid(), SIGTERM)
    }
}

if enterRunLoop {
    RunLoop.main.run()
}
