//
//  ManuscriptDocument.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Cocoa
import SwiftUI
import Combine
import os.log

enum DocumentType: String {
    case managed = "io.manuscripts.project-reference"
    case standalone = "io.manuscripts.project"

    var documentClass: ProjectDocument.Type {
        switch self {
        case .managed:
            return ManagedProjectDocument.self
        case .standalone:
            return StandaloneProjectDocument.self
        }
    }
}

@objc
class ProjectDocument: NSDocument {

    /**
     Editor content refering to a project and a primary action (e.g. open/new).

     This property has to be set before `makeWindowControllers()` is called.
     */
    var content: EditorContent? {
        willSet {
            contentSink = nil
        }
        didSet {
            contentSink = observeEditorContent()
        }
    }

    override func makeWindowControllers() {
        guard let actualContent = content else {
            preconditionFailure("can not create a document without content")
        }
        let windowController = ProjectWindowController(content: actualContent)
        addWindowController(windowController)
    }

    private var contentSink: AnyCancellable?
    private var projectSink: AnyCancellable?

    var nativeBridge: NativeBridge? {
        mainWindowController?.editorViewControllerProxy?.editorViewController.nativeBridge
    }

    private func observeEditorContent() -> AnyCancellable {
        guard let editorContent = content else { preconditionFailure("can not observe nil content") }
        return editorContent.$project.sink { [weak self] nextProject in
            guard let this = self else { return }
            if let project = nextProject {
                this.projectSink = this.observeContentProject(project)
                this.noticeNextProject(project)
            } else {
                this.projectSink = nil
            }
        }
    }

    private func observeContentProject(_ project: Project) -> AnyCancellable {
        return project.objectWillChange.sink { [weak self] _ in
            self?.noticeProjectChange()
        }
    }

    func noticeNextProject(_ project: Project) {
        //noop
    }

    func noticeProjectChange() {
        //noop
    }

    // MARK: - Scripting
    @objc dynamic var mainWindowController: ProjectWindowController? {
        windowControllers.first as? ProjectWindowController
    }

}

// MARK: -
@objc
class ManagedProjectDocument: ProjectDocument {

    override func data(ofType typeName: String) throws -> Data {
        // Insert code here to write your document to data of the specified type, throwing an error in case of failure.
        // Alternatively, you could remove this method and override fileWrapper(ofType:), write(to:ofType:), or write(to:ofType:for:originalContentsURL:) instead.
        throw NSError(domain: NSOSStatusErrorDomain, code: unimpErr, userInfo: nil)
    }

    override func read(from data: Data, ofType typeName: String) throws {
        // Insert code here to read your document from the given data of the specified type, throwing an error in case of failure.
        // Alternatively, you could remove this method and override read(from:ofType:) instead.  If you do, you should also override isEntireFileLoaded to return false if the contents are lazily loaded.
        throw NSError(domain: NSOSStatusErrorDomain, code: unimpErr, userInfo: nil)
    }

    override class var autosavesInPlace: Bool {
        return false
    }

    override class var autosavesDrafts: Bool {
        return false
    }

    override var isDocumentEdited: Bool {
        return false
    }

    override func read(from url: URL, ofType typeName: String) throws {
        //noop
    }

    override func write(to url: URL, ofType typeName: String) throws {
        //noop        
    }

    override func noticeNextProject(_ project: Project) {
        DispatchQueue.main.async {
            do {
                try self.revert(toContentsOf: project.url, ofType: self.fileType ?? "")
            } catch let error {
                os_log(.error, "could not revert a document: %{public}@", error.localizedDescription)
            }
        }
    }

    override func noticeProjectChange() {
        DispatchQueue.main.async {
            guard
                let project = self.content?.project
            else { return }
            do {
                try self.revert(toContentsOf: project.url, ofType: self.fileType ?? "")
            } catch let error {
                os_log(.error, "could not revert a document: %{public}@", error.localizedDescription)
            }
        }
    }

}

// MARK: -
class StandaloneProjectDocument: ProjectDocument {

    override init() {
        super.init()
    }

    override class var autosavesInPlace: Bool {
        return true
    }

    override func read(from url: URL, ofType typeName: String) throws {
        let archiver = ProjectArchiver()
        let project = try archiver.unarchiveProject(from: url, documentServer: AppEnvironment.shared.dataManager[.standalone].documentServer)

        content = EditorContent(project: project)
    }

    override func write(to url: URL, ofType typeName: String) throws {
        guard let project = content?.project else {
            unblockUserInteraction()
            return
        }

        let archiver = ProjectArchiver()
        try archiver.archive(project: project, to: url) {
            self.unblockUserInteraction()
        }
    }

    /**
     We override this method to enforce proper CBL concurrency policies.

     `NativeBridge.performSyncIdle(block:)` calls back on the main thread (hence serialize responses).
     As all communication between FE and the app happens on the  MT we are blocking any changes to CBL
     coming from FE until `performSyncIdle` block is executing.

     In addition `super.save(to:ofType:for:completionHandler` blocks all UI (a.k.a. MT)
     until `NSDocument.unblockUserInteraction()` is called. (See `NSDocument` documentation).

     As we support concurrent and safe saving `ProjectDocument.write(to:ofType:)` is called on a new thread
     but MT is still blocked.

     The ProjectArchive calls back at the moment the transaction reading project data is started - what is a safe point
     to unblock UI by calling `NSDocument.unblockUserInteraction`.
     */
    override func save(to url: URL, ofType typeName: String, for saveOperation: NSDocument.SaveOperationType, completionHandler: @escaping (Error?) -> Void) {
        nativeBridge?.performSyncIdle {
            super.save(to: url, ofType: typeName, for: saveOperation, completionHandler: completionHandler)
        }
    }

    override func canAsynchronouslyWrite(to url: URL, ofType typeName: String, for saveOperation: NSDocument.SaveOperationType) -> Bool {
        return true
    }

}
