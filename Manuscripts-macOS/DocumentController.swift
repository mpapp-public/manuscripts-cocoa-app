//
//  DocumentController.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import AppKit
import os.log

class DocumentController: NSDocumentController {

    enum DocumentError: Error {
        case canNotCreateUntitledDocument
        case unsupportedUrl
        case unsupportedContent
    }

    override var defaultType: String? {
        return DocumentType.standalone.rawValue
    }

    /*
    @discardableResult
    func newEditor(in enclave: DataEnclave) throws -> NSDocument {
        guard let document = try makeUntitledDocument(ofType: "io.manuscripts.project-reference") as? ProjectDocument else {
            preconditionFailure("could not create document (Info.plist problem?)")
        }

        addDocument(document)
        document.content = EditorContent(enclave: enclave)
        document.makeWindowControllers()
        document.showWindows()

        return document
    }
     */

    @discardableResult
    func open(content: EditorContent) throws -> NSDocument {
        switch content.action {
        case .openProject:
            guard let url = content.project?.url else {
                preconditionFailure("no url for a project being opened")
            }
            //already opened?
            if let openedDocument = document(for: url) {
                openedDocument.showWindows()
                return openedDocument
            }

            guard let document = try makeDocument(withContentsOf: url, ofType: content.documentType) as? ProjectDocument else {
                preconditionFailure("could not create document (Info.plist problem?)")
            }

            addDocument(document)
            document.content = content
            document.makeWindowControllers()
            document.showWindows()
            return document
        case .newProject:
            guard let document = try makeUntitledDocument(ofType: content.documentType) as? ProjectDocument else {
                preconditionFailure("could not create document (Info.plist problem?)")
            }

            addDocument(document)
            document.content = content
            document.makeWindowControllers()
            document.showWindows()
            return document
        default:
            throw DocumentError.unsupportedContent
        }
    }

    func closeDocument(with content: EditorContent) {
        if let closingDocument = documents.first(where: { ($0 as? ProjectDocument)?.content === content }) {
            closingDocument.close()
        }
    }

    static override func restoreWindow(withIdentifier identifier: NSUserInterfaceItemIdentifier, state: NSCoder, completionHandler: @escaping (NSWindow?, Error?) -> Void) {
        //no restoration yet
        completionHandler(nil, nil)
    }

    override func openUntitledDocumentAndDisplay(_ displayDocument: Bool) throws -> NSDocument {
        let enclave = AppEnvironment.shared.dataManager[.standalone]
        let content = EditorContent(enclave: enclave, action: .newProject)
        return try open(content: content)
    }

    /*
    override func presentError(_ error: Error) -> Bool {
        switch error {
        case DocumentError.canNotCreateUntitledDocument:
            return true
        default:
            return super.presentError(error)
        }
    }
     */

}
