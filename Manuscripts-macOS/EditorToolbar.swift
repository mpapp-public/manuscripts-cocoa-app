//
//  EditorToolbar.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import AppKit
import Combine
import os.log

extension ToolbarItem {
    static var groupIdentifiers: [NSToolbarItem.Identifier] {
        groups.map { (idString, _) in
            NSToolbarItem.Identifier(rawValue: idString)
        }
    }
}

class EditorToolbar: NSToolbar {
    private let toolbarState: ToolbarState
    private var toolbarStateSink: AnyCancellable?

    init(identifier: String, toolbarState: ToolbarState) {
        self.toolbarState = toolbarState
        super.init(identifier: identifier)

        self.delegate = self
        allowsUserCustomization = true
        displayMode = .iconOnly
        sizeMode = .small
        autosavesConfiguration = true

        toolbarStateSink = toolbarState.objectWillChange
            .receive(on: DispatchQueue.main)
            .sink { [weak self] _ in
                self?.validateVisibleItems()
            }
    }

    override func validateVisibleItems() {
        visibleItems?.forEach { (item) in
            guard
                let itemGroup = item as? NSToolbarItemGroup,
                let segmentedControl = itemGroup.view?.firstDescendant(of: NSSegmentedControl.self)
            else {
                return
            }
            for (itemIndex, singleItem) in itemGroup.subitems.enumerated() {
                guard let item = ToolbarItem(rawValue: singleItem.itemIdentifier.rawValue) else {
                    break
                }
                let itemState = toolbarState[item]
                segmentedControl.setEnabled(itemState.isEnabled, forSegment: itemIndex)
                if itemGroup.selectionMode != .momentary {
                    itemGroup.setSelected(itemState.isActive, at: itemIndex)
                }
            }
        }
    }

}

extension EditorToolbar: NSToolbarDelegate {

    func toolbarAllowedItemIdentifiers(_ toolbar: NSToolbar) -> [NSToolbarItem.Identifier] {
        return [.flexibleSpace, .space] + ToolbarItem.groupIdentifiers
    }

    func toolbarDefaultItemIdentifiers(_ toolbar: NSToolbar) -> [NSToolbarItem.Identifier] {
        return ToolbarItem.groupIdentifiers
    }

    func toolbar(_ toolbar: NSToolbar, itemForItemIdentifier itemIdentifier: NSToolbarItem.Identifier, willBeInsertedIntoToolbar flag: Bool) -> NSToolbarItem? {

        guard let group = ToolbarItem.groups.first(where: {$0.key == itemIdentifier.rawValue }) else {
            return nil
        }

        let itemGroup = NSToolbarItemGroup(itemIdentifier: itemIdentifier)
        itemGroup.autovalidates = false
        itemGroup.isBordered = true //uses NSSegmentedControl
        itemGroup.controlRepresentation = .expanded
        itemGroup.label = group.value.label
        switch group.value.selection {
        case .momentary:
            itemGroup.selectionMode = .momentary
        case .single:
            itemGroup.selectionMode = .selectOne
        case .multiple:
            itemGroup.selectionMode = .selectAny
        }

        itemGroup.subitems = group.value.items.map { item in
            let toolbarItem = NSToolbarItem(itemIdentifier: NSToolbarItem.Identifier(rawValue: item.rawValue))
            toolbarItem.autovalidates = false
            toolbarItem.isEnabled = false
            toolbarItem.target = self
            toolbarItem.action = #selector(toolbarItemAction(_:))
            if let imageName = item.imageName {
                toolbarItem.image = NSImage(named: imageName)
            }
            return toolbarItem
        }

        return itemGroup
    }

    @objc private func toolbarItemAction(_ toolbarItem: NSToolbarItem) {
        guard let item = ToolbarItem(rawValue: toolbarItem.itemIdentifier.rawValue) else {
            os_log(.error, "unsupported toolbar item action %{public}@", toolbarItem.itemIdentifier.rawValue)
            return
        }

        toolbarState.signalAction(for: item)
    }

}
