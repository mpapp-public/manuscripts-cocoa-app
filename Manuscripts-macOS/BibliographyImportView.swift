//
//  BibliographyImportView.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import SwiftUI

/**
 Sheet view used for bibliography import.
 */
struct BibliographyImportView: View {
    @EnvironmentObject var interactiveContext: InteractiveContext
    @EnvironmentObject var interactiveTask: InteractiveTask

    @State var showDetails = false

    var body: some View {
        // swiftlint:disable force_cast
        let importTask = interactiveTask as! BibliographyImportTask

        return VStack(alignment: .leading, spacing: 20) {
            Text("Importing bibliography")
            ProgressView(value: $interactiveTask.fractionCompleted)
                .frame(height: CGFloat(10))
            if importTask.importState.lastError != nil { Text(importTask.importState.lastError?.localizedDescription ?? "unknown error")
                    .lineLimit(nil)
                    .foregroundColor(.red)
                    .fixedSize(horizontal: false, vertical: true)
            }
            if importTask.importState.conclusion != nil {
                Text(importTask.importState.conclusion ?? "")
                    .lineLimit(nil)
                    .fixedSize(horizontal: false, vertical: true)
            }
            if importTask.importState.details != nil {
                VStack(alignment: .leading) {
                    Button(action: { self.showDetails.toggle() }, label: {
                        Text("Details")
                    })
                        .buttonStyle(PlainButtonStyle())
                    if self.showDetails {
                        ScrollView {
                            HStack {
                                Text(importTask.importState.details ?? "")
                                    .lineLimit(nil)
                                    .padding()
                                Spacer()
                            }
                        }
                        .frame(maxHeight: 300)
                        .fixedSize(horizontal: false, vertical: true)
                        .background(Color.white)
                    }
                }
            }
            HStack {
                Spacer()
                ForEach(importTask.importState.buttons ?? []) { button in
                    Button(action: { button.action() }, label: {
                        Text(button.label)
                    })
                        .disabled(self.interactiveTask.state == .running)
                }
                Button(action: {self.interactiveTask.cancel()}, label: {
                    Text(interactiveTask.state == .finished ? "Close" : "Cancel")
                })
            }
        }
        .frame(width: 400)
        .padding()
        .onAppear {
            self.interactiveTask.start()
        }
    }
}

#if DEBUG
struct BookendsImportViewPreviews: PreviewProvider {

    class TestBibliographyImport: BibliographyImport, InteractiveTaskProvider {
        var sheetView: AnyView {
            AnyView(EmptyView())
        }
    }

    static var previews: some View {
        let context = InteractiveContext()
        let task = BibliographyImportTask(importProvider: TestBibliographyImport(), nativeBridge: NativeBridge())
        context.interactiveTask = task

        task.importState.conclusion = "This is some debug conclusion"
        let manyLines = (1...100).map({"error description line: \($0)"}).joined(separator: "\n")
        task.importState.details = manyLines

        let plainTask: InteractiveTask = task

        return BibliographyImportView()
            .environmentObject(context)
            .environmentObject(plainTask)
    }
}
#endif
