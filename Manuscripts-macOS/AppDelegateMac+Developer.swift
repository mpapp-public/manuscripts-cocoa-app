//
//  AppDelegateMac+Developer.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

#if DEBUG

import Foundation
import WebKit
import SwiftUI
import CouchbaseLite
import ZIPFoundation

extension AppDelegateMac {

    @IBAction func forceLoginReset(_ sender: Any) {
        //erase all cookies to force API not to share identity tokens with connect.science
        WKWebsiteDataStore.default().removeData(ofTypes: ["WKWebsiteDataTypeCookies"], modifiedSince: Date.distantPast) {
            self.sessionManager.requestLogin()
        }
    }

    @IBAction func dumpStorage(_ sender: Any) {

        //let types: Set<String> = ["WKWebsiteDataTypeCookies"]
        let types: Set<String> = WKWebsiteDataStore.allWebsiteDataTypes()

        WKWebsiteDataStore.default().fetchDataRecords(ofTypes: types) { (records) in
            print("here we are: \(records.first!)")
        }
    }

    @IBAction func createLocalProjectStub(_ sender: Any) {
        do {
            guard let manager = dataManager[.local].documentServer.manager else {
                preconditionFailure("documentServer without a manager")
            }
            let database = try manager.databaseNamed(DocumentServer.userDatabaseName)

            let userProfile = database["MPProject:E206F723-6BA9-4D14-AE3F-A945612065EF"]

            try userProfile?.putProperties([
                "objectType": "MPProject",
                "owners": ["User_FDD56078-8A93-4239-ABC8-9C8BD3F4EA67"],
                //"sessionID":"18672fe7-9f02-4fd6-8170-7bd1f8a6a9eb",
                "title": "About Manuscripts",
                "viewers": [],
                "writers": []
            ])
        } catch let err {
            NSApp.presentError(err)
        }
    }

    @IBAction func showProjectManager(_ sender: Any) {
        let projectManagerView = ProjectManagerView()
        let hosting = NSHostingController(rootView: projectManagerView)
        let projectManagerWindow = NSWindow(contentViewController: hosting)
        projectManagerWindow.setFrameAutosaveName("ProjectManager")
        projectManagerWindow.styleMask = [.resizable, .titled, .closable]
        projectManagerWindow.title = "Projects"
        projectManagerWindow.makeKeyAndOrderFront(NSApp)
    }

    @IBAction func dumpScriptingRegistry(_ sender: Any) {
        let registry = NSScriptSuiteRegistry.shared()
        let classes = registry.classDescriptions(inSuite: "Manuscripts Document Content Suite")
        print("\(String(describing: classes))")
    }

    @IBAction func executeAppleScript(_ sender: Any) {
        //empty
    }

    @IBAction func testImporterBigFileChunked(_ sender: Any) {
        let importer = BookendsBibliographyImport()
        //importer.processByChunk = true

        guard let fileUrl = Bundle.main.url(forResource: "sample-big", withExtension: "ris") else {
            //XCTFail("file not found")
            return
        }

        //let expect = expectation(description: "expect import")
        importer.importLibrary(from: fileUrl) { _ in
            //XCTAssertNotNil(chunks)
            //XCTAssertEqual(chunks?.count ?? 0, 200)
            //expect.fulfill()
        }

        //waitForExpectations(timeout: 1000)
    }

    @IBAction func purgeLocalData(_ sender: Any) {
        do {
            let sessionState = AppEnvironment.shared.sessionManager.localSessionState
            try AppEnvironment.shared.dataManager.localEnclave.reset(to: sessionState)
            try (AppEnvironment.shared.dataManager.localEnclave.documentServer as? LocalDocumentServer)?.setupLocalSession()
        } catch let error {
            NSApp.presentError(error)
        }

        /*
        let manager = WebKitDataManager()
        let urls = try? manager.resourceURLsOfTypes(WebKitDataManager.DataType.allTypes, for: EditorWebViewCoordinator.localBootstrapBaseUrl)
        print("urls: \(String(describing: urls))")
         */

        /*
        //let recordTypes: Set<String> = [WKWebsiteDataTypeIndexedDBDatabases]
        let recordTypes = WKWebsiteDataStore.allWebsiteDataTypes()
        WKWebsiteDataStore.default().fetchDataRecords(ofTypes: recordTypes) { records in
            let url = URL(string: Configuration.standard.apiBaseUrl)!

            /*
            let cloudRecords = records.filter {
                !$0.displayName.isEmpty && (url.host ?? "__never_match__").hasSuffix($0.displayName)
            }
             */

            /*
            WKWebsiteDataStore.default().removeData(ofTypes: WKWebsiteDataStore.allWebsiteDataTypes(), for: records) {
                print("all erased")
            }
             */

            /*
            assert(cloudRecords.count <= 1, "unexpected number of cloud records in WKWebsiteDataStore")
            WKWebsiteDataStore.default().removeData(ofTypes: WKWebsiteDataStore.allWebsiteDataTypes(), for: cloudRecords) {
                promise(.success(true))
            }
             */
        }
         */
    }

    //swiftlint:disable function_body_length cyclomatic_complexity
    @IBAction func testDumpStandaloneProject(_ sender: Any) {
        guard
            let document = DocumentController.shared.documents.first as? ProjectDocument,
            let project = document.content?.project
        else {
            return
        }

        guard let manager = project.enclave.documentServer.manager else {
            preconditionFailure("documentServer without a manager")
        }

        let name = (project.url.lastPathComponent as NSString).deletingPathExtension

        guard let database = try? manager.existingDatabaseNamed(name) else {
            return
        }

        database.inTransaction {
            do {
                let query = database.createAllDocumentsQuery()
                let enumerator = try query.run()

                //create an archive
                let url = URL(fileURLWithPath: "/Users/pavel/Desktop/test.zip")
                try? FileManager.default.removeItem(at: url)
                guard let archive = Archive(url: url, accessMode: .create) else {
                    return true
                }

                //write header
                var indexDict = [String: Any]()
                indexDict["version"] = 1
                let indexData = try JSONSerialization.data(withJSONObject: indexDict, options: [])

                try archive.addEntry(with: "index.json", type: .file, uncompressedSize: UInt32(indexData.count), compressionMethod: .deflate) { (position, size) in
                    return indexData[position..<(position + size)]
                }

                var attachmentFiles = Set<String>()

                var documents = [[String: Any]]()
                for item in enumerator {
                    guard
                        let row = item as? CBLQueryRow,
                        let document = row.document
                    else { continue }

                    let history = try document.getRevisionHistory()
                    var allRevisions = [[String: Any]]()

                    var dict = [String: Any]()
                    var collectedAttachments = [String: String]()
                    dict["documentId"] = document.documentID
                    //aggregate attchments
                    for revision in history {
                        guard let props = revision.properties else { break }
                        allRevisions.append(props)

                        guard let attachments = revision.attachments else {
                            continue
                        }
                        for attachment in attachments {
                            guard
                                let revId = revision.revisionID,
                                let attachmentUrl = attachment.contentURL
                            else {
                                continue
                            }
                            collectedAttachments["\(revId)-\(attachment.name)"] = attachmentUrl.lastPathComponent
                            attachmentFiles.insert(attachmentUrl.lastPathComponent)
                        }

                    }
                    if !collectedAttachments.isEmpty {
                        dict["attachments"] = collectedAttachments
                    }
                    dict["revisions"] = allRevisions
                    //append data
                    documents.append(dict)
                }
                //store documents/revisions
                let documentsData = try JSONSerialization.data(withJSONObject: documents, options: [.prettyPrinted])
                try archive.addEntry(with: "content.json", type: .file, uncompressedSize: UInt32(documentsData.count), compressionMethod: .deflate) { (position, size) in
                    return documentsData[position..<(position + size)]
                }
                //store all attachments
                for attachmentFile in attachmentFiles {
                    try archive.addEntry(with: "attachments/\(attachmentFile)", relativeTo: project.url)
                }
            } catch let err {
                print("error while dumping documents: \(err.localizedDescription)")
            }
            return true
        }
    }

    @IBAction
    func testLoadArchive(_ sender: Any) {
        guard
            let documentServer = AppEnvironment.shared.dataManager[.standalone].documentServer,
            let manager = documentServer.manager
        else {
            preconditionFailure("preconditions not met")
        }

        var userProfile: CBLDocument!
        manager.doSync {
            userProfile = try? manager.fetchUserProfile()
        }
        let userId = userProfile.properties?["userID"] as? String

        let projectUuid = UUID()

        //swiftlint:disable force_try
        let userDatabase = try! manager.databaseNamed("user")
        let projectDocumentId = "MPProject:\(projectUuid.uuidString)"
        let projectDocument = userDatabase.document(withID: projectDocumentId)!
        let projectProperties: [String: Any] = ["objectType": "MPProject", "owners": [userId]]
        try! projectDocument.putProperties(projectProperties)

        let project = Project(documentId: projectDocumentId, dict: projectProperties, enclave: AppEnvironment.shared.dataManager[.standalone])!

        let name = (project.url.lastPathComponent as NSString).deletingPathExtension

        guard let database = try? manager.databaseNamed(name) else {
            preconditionFailure("could not create database")
        }

        let url = URL(fileURLWithPath: "/Users/pavel/Desktop/test.zip")
        guard let archive = Archive(url: url, accessMode: .read) else {
            preconditionFailure("could not create archive")
        }

        database.inTransaction {
            do {
                let indexData = try archive.extract(path: "index.json")
                let indexJson = try JSONSerialization.jsonObject(with: indexData, options: [])

                // TODO: check version here
                print("index: \(indexJson)")

                let contentData = try archive.extract(path: "content.json")
                guard let contentJson = try JSONSerialization.jsonObject(with: contentData, options: []) as? [[String: Any]] else {
                    // TODO: just throw here
                    preconditionFailure("invalid format")
                }

                for documentJson in contentJson {
                    guard
                        let documentId = documentJson["documentId"] as? String,
                        let revisions = documentJson["revisions"] as? [[String: Any]]
                    else {
                        // TODO: just throw here
                        preconditionFailure("invalid format")
                    }
                    let document = database.document(withID: documentId)
                    let attachmentMappings = documentJson["attachments"] as? [String: Any]

                    var revHistory = [String]()
                    for var revisionJson in revisions {
                        revisionJson["containerID"] = projectDocumentId
                        //swiftlint:disable force_cast
                        let revId = revisionJson["_rev"] as! String
                        // TODO: typecheck and throw
                        revHistory.insert(revId, at: 0)
                        //revHistory.append(revisionJson["_rev"] as! String)

                        //collect attachments
                        var attachmentBodies = [String: Data]()
                        if let attachmentsJson = revisionJson["_attachments"] as? [String: Any] {
                            for attachmentName in attachmentsJson.keys {
                                guard
                                    let bodyFileName = attachmentMappings?["\(revId)-\(attachmentName)"] as? String
                                else {
                                    preconditionFailure("attachment not found")
                                }
                                let attachmentBody = try archive.extract(path: "attachments/\(bodyFileName)")
                                attachmentBodies[attachmentName] = attachmentBody
                            }
                        }

                        try document?.putExistingRevision(withProperties: revisionJson, attachments: attachmentBodies, revisionHistory: revHistory, from: nil)
                    }

                }
            } catch let err {
                print("error reading document: \(err.localizedDescription)")
            }

            DispatchQueue.main.async {
                let content = EditorContent(project: project)
                try! (NSDocumentController.shared as! DocumentController).open(content: content)
            }

            return true
        }
        print("done")
    }

}

extension Archive {
    func extract(path: String) throws -> Data {
        var data = Data()
        guard let entry = self[path] else {
            // TODO: throw here
            preconditionFailure("file not found in archive")
        }
        _ = try extract(entry) { chunk in
            data.append(chunk)
        }
        return data
    }
}

#endif
