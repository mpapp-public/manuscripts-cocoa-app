//
//  EditorViewController.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import SwiftUI
import os.log

/**
 Concrete NSHostingController subclass tying NSDocument architecture, OSA scripting and SwiftUI together.
 */
class EditorViewController: NSHostingController<AnyView> {
    static let defaultContentSize = CGSize(width: 800, height: 600)

    let nativeBridge = NativeBridge()
    let interactiveContext = InteractiveContext()

    convenience init(content: EditorContent, toolbarState: ToolbarState, mainMenuState: MainMenuState) {
        self.init(rootView: AnyView(EmptyView()))

        let contentView = EditorView()
            .environmentObject(content)
            .environmentObject(toolbarState)
            .environmentObject(mainMenuState)
            .environmentObject(nativeBridge)
            .environmentObject(interactiveContext)

        rootView = AnyView(contentView)
    }

    @IBAction func importBookendsLibrary(_ sender: Any) {
        interactiveContext.interactiveTask = BibliographyImportTask(importProvider: BookendsBibliographyImport(), nativeBridge: nativeBridge)
    }
}

/**
 Scriptable proxy to EditorViewController.

 This class is needed because we can not inherit a scriptable object (@objc) from a Swift generic class (NSHostingController)
 */
@objc
public class EditorViewControllerScriptingProxy: NSObject {
    let editorViewController: EditorViewController

    weak var windowController: ProjectWindowController?

    init(windowController: ProjectWindowController, editorViewController: EditorViewController) {
        self.windowController = windowController
        self.editorViewController = editorViewController
        super.init()
    }

    override public var objectSpecifier: NSScriptObjectSpecifier? {
        guard
            let windowSpecifier = windowController?.objectSpecifier,
            let classDescription = windowSpecifier.keyClassDescription
            else {
                return nil
        }
        let specifier = NSPropertySpecifier(containerClassDescription: classDescription, containerSpecifier: windowSpecifier, key: "editorViewControllerProxy")
        return specifier
    }

    @objc(handleCiteCommand:)
    public func cite(with command: NSScriptCommand) {
        editorViewController.nativeBridge.cite(with: command)
    }

}
