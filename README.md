# manuscripts-cocoa-app

Manuscripts.io client app for for macOS and iOS.

# Build prerequisites

- [Node.js](https://nodejs.org) 10.x or later.
- [yarn](https://yarnpkg.com)
- Xcode 11 or later.

# Build instructions

1. `git submodule update --init --recursive` to update the Git submodule references.
2. Run `./Script/build-dependencies.sh` to update the bundled JS packages and data.
3. Build with Xcode.
