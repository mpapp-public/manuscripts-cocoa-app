//
//  AppleScriptTypes.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import Carbon

protocol AppleEventKeywordConvertible: Hashable {
    var appleEventKeyword: AEKeyword { get }
}

// swiftlint:disable all
enum AppleEventPreposition: AppleEventKeywordConvertible {
    case at
    case `in`
    case from
    case `for`
    case to
    case thru
    case through
    case by
    case on
    case into
    case onto
    case between
    case against
    case outOf
    case insteadOf
    case asideFrom
    case around
    case beside
    case beneath
    case under
    case over
    case above
    case below
    case apartFrom
    case given
    case with
    case without
    case about
    case since
    case until

    var appleEventKeyword: AEKeyword {
        switch self {
        case .at: return AEKeyword(keyASPrepositionAt)
        case .in: return AEKeyword(keyASPrepositionIn)
        case .from: return AEKeyword(keyASPrepositionFrom)
        case .for: return AEKeyword(keyASPrepositionFor)
        case .to: return AEKeyword(keyASPrepositionTo)
        case .thru: return AEKeyword(keyASPrepositionThru)
        case .through: return AEKeyword(keyASPrepositionThrough)
        case .by: return AEKeyword(keyASPrepositionBy)
        case .on: return AEKeyword(keyASPrepositionOn)
        case .into: return AEKeyword(keyASPrepositionInto)
        case .onto: return AEKeyword(keyASPrepositionOnto)
        case .between: return AEKeyword(keyASPrepositionBetween)
        case .against: return AEKeyword(keyASPrepositionAgainst)
        case .outOf: return AEKeyword(keyASPrepositionOutOf)
        case .insteadOf: return AEKeyword(keyASPrepositionInsteadOf)
        case .asideFrom: return AEKeyword(keyASPrepositionAsideFrom)
        case .around: return AEKeyword(keyASPrepositionAround)
        case .beside: return AEKeyword(keyASPrepositionBeside)
        case .beneath: return AEKeyword(keyASPrepositionBeneath)
        case .under: return AEKeyword(keyASPrepositionUnder)
        case .over: return AEKeyword(keyASPrepositionOver)
        case .above: return AEKeyword(keyASPrepositionAbove)
        case .below: return AEKeyword(keyASPrepositionBelow)
        case .apartFrom: return AEKeyword(keyASPrepositionApartFrom)
        case .given: return AEKeyword(keyASPrepositionGiven)
        case .with: return AEKeyword(keyASPrepositionWith)
        case .without: return AEKeyword(keyASPrepositionWithout)
        case .about: return AEKeyword(keyASPrepositionAbout)
        case .since: return AEKeyword(keyASPrepositionSince)
        case .until: return AEKeyword(keyASPrepositionUntil)
        }
    }
}
// swiftlint:enable all

protocol AppleEventDescriptorConvertible {
    func appleEventDescriptor() -> NSAppleEventDescriptor
}

extension String: AppleEventDescriptorConvertible {
    func appleEventDescriptor() -> NSAppleEventDescriptor {
        NSAppleEventDescriptor(string: self)
    }
}

extension Int32: AppleEventDescriptorConvertible {
   func appleEventDescriptor() -> NSAppleEventDescriptor {
        NSAppleEventDescriptor(int32: self)
    }
}

extension Array: AppleEventDescriptorConvertible where Element == AppleEventDescriptorConvertible {
    func appleEventDescriptor() -> NSAppleEventDescriptor {
        let result = NSAppleEventDescriptor.list()
        for item in self {
            result.insert(item.appleEventDescriptor(), at: 0)
        }
        return result
    }
}

extension Dictionary: AppleEventDescriptorConvertible where Key: StringProtocol, Value == AppleEventDescriptorConvertible {
    func appleEventDescriptor() -> NSAppleEventDescriptor {
        let result = NSAppleEventDescriptor.list()
        for (key, value) in self {
            result.insert(String(key).appleEventDescriptor(), at: 0)
            result.insert(value.appleEventDescriptor(), at: 0)
        }
        return result
    }
}

extension Dictionary where Key: AppleEventKeywordConvertible, Value == AppleEventDescriptorConvertible {
    func setEventParameters(_ event: NSAppleEventDescriptor) {
        for (key, value) in self {
            event.setParam(value.appleEventDescriptor(), forKeyword: key.appleEventKeyword)
        }
    }
}
