//
//  ScriptingConnector.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import Carbon

/**
 Wraps an Apple script provided as a file or a string. Provides interface to call a AS function with parameters.
 */
class AppleScriptWrapper {

    enum ScriptingError: LocalizedError {
        case appleScriptError([String: Any]?)
    }

    let appleScript: NSAppleScript

    init(scriptUrl: URL) throws {
        var errorDict: NSDictionary?
        guard let script = NSAppleScript(contentsOf: scriptUrl, error: &errorDict) else {
            throw ScriptingError.appleScriptError(errorDict as? [String: Any])
        }
        appleScript = script
    }

    init(source: String) throws {
        guard let script = NSAppleScript(source: source) else {
            throw ScriptingError.appleScriptError(nil)
        }
        appleScript = script
    }

    func call(function name: String, directObject: [AppleEventDescriptorConvertible]? = nil, keywordParams: [AppleEventPreposition: AppleEventDescriptorConvertible]? = nil, userParams: [String: AppleEventDescriptorConvertible]? = nil) throws -> NSAppleEventDescriptor {

        let target = NSAppleEventDescriptor.currentProcess()
        let function = NSAppleEventDescriptor(string: name)

        let functionDescriptor = NSAppleEventDescriptor.appleEvent(withEventClass: AEEventClass(kASAppleScriptSuite), eventID: AEEventID(kASSubroutineEvent), targetDescriptor: target, returnID: AEReturnID(kAutoGenerateReturnID), transactionID: AETransactionID(kAnyTransactionID))

        functionDescriptor.setParam(function, forKeyword: AEKeyword(keyASSubroutineName))

        //direct (a.k.a. positional) parameters
        if let direct = directObject {
            functionDescriptor.setParam(direct.appleEventDescriptor(), forKeyword: AEKeyword(keyDirectObject))
        }

        //preposition keyword parameters
        if let keyword = keywordParams {
            keyword.setEventParameters(functionDescriptor)
        }

        //given user named parameters
        if let user = userParams {
            functionDescriptor.setParam(user.appleEventDescriptor(), forKeyword: AEKeyword(keyASUserRecordFields))
        }

        var errorDictionary: NSDictionary?
        guard let returnValue = appleScript.executeAppleEvent(functionDescriptor, error: &errorDictionary) as NSAppleEventDescriptor? else {
            throw ScriptingError.appleScriptError(errorDictionary as? [String: Any])
        }
        return returnValue
    }

}
